package api;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.api.aspire.common.constant.ConciergeCaseType;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;
import com.api.aspire.domain.usecases.AskConciergeAspire;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.ResetPassword;
import com.api.aspire.domain.usecases.RetrievePassword;
import com.api.aspire.domain.usecases.SaveProfile;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.domain.mapper.profile.MapProfile;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;
import com.mastercard.usdemo.domain.usecases.UDAUserCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.reactivex.Single;
import io.reactivex.observers.DisposableCompletableObserver;

import static org.junit.Assert.assertTrue;


/**
 * JUnit4 unit tests for logic.
 */
@RunWith(AndroidJUnit4.class)
public class ProfileApiTest {

    private String TAG = "ProfileApiTest";
    private UDAUserCase getUserApp;
    private LoadProfile loadProfile;
    private MapProfile mapProfile;
    private MapProfileApp mapProfileApp;
    private GetAccessToken getAccessToken;
    private SaveProfile saveProfile;
    private ResetPassword changePassword;
    private RetrievePassword retrievePassword;
    private CheckPassCode checkPassCode;
    private AskConciergeAspire askConciergeApi;

    private Context context;

    private boolean rsTest;
    private CreateProfileCase createProfileCase;


    private final int PASS_CODE_VALID = BuildConfig.AS_BIN;
    private String email = "tictacpcudaaspire1@gmail.com";
    private String pwd = "Admin@1234";

    /*list account: tictacpcuda15@gmail.com */

    @Before
    public void setUp() {
//        this.context = App.getInstance().getApplicationContext();
//        this.mapProfileApp = new MapProfileApp();
//        this.mapProfile = new MapProfile();
//        this.getUserApp = new UDAUserCase(context);
//        this.createProfileCase = new CreateProfileCase(context, mapProfileApp);
//        this.loadProfile = new LoadProfile(context,mapProfileApp);
//        this.getAccessToken = new GetAccessToken(context,mapProfileApp);
//        this.saveProfile = new SaveProfile(context,mapProfileApp);
//        this.changePassword = new ResetPassword(context, mapProfileApp);
//        this.retrievePassword = new RetrievePassword(mapProfileApp);
//
//        AuthAspireRepository repository = new AuthDataAspireRepository();
//        GetToken getToken = new GetToken(context, mapProfileApp);
//        this.checkPassCode = new CheckPassCode(
//                context,
//                mapProfileApp,
//                repository,
//                getAccessToken,
//                loadProfile,
//                saveProfile,
//                getToken, BuildConfig.AS_TOKEN_USERNAME, BuildConfig.AS_TOKEN_SECRET);
//
//        //Request Concierge case
//        this.askConciergeApi = new AskConciergeAspire(context, mapProfileApp);

    }

    //<editor-fold desc="Sign In">
    /**
     * Sign In for project BDA, MDA
     * */
    @Test
    public void signIn_CheckPassCode_ReturnSuccess() throws Exception {
        rsTest = false;
//        getUserApp.signIn(new SignInCase.Params(email, pwd))
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        Log.d(TAG, "//---------- onComplete: Sign In done");
//                        rsTest = true;
//                        dispose();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (ErrCode.PASS_CODE_ERROR.name().equals(e.getMessage())
//                                || ErrCode.PASS_CODE_INVALID_ERROR.name().equals(e.getMessage())) {
//                            Log.d(TAG, "//---------- onError: SignIn passCode");
//                        } else if (ErrorApi.isGetTokenError(e.getMessage())) {
//                            Log.d(TAG, "//---------- onError: " + App.getInstance().getString(R.string.errorSignIn));
//                        } else {
//                            Log.d(TAG, "//---------- onError: " + e.getMessage());
//                        }
//                        rsTest = false;
//                        dispose();
//                    }
//                });
//        //result
//        assertTrue(rsTest);
    }

    @Test
    public void signIn_InvalidPass_ReturnError() throws Exception {
        rsTest = false;
//        getUserApp.signIn(new SignInCase.Params(email, pwd))
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        Log.d(TAG, "//---------- onComplete: Sign In done");
//                        rsTest = true;
//                        dispose();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (ErrCode.PASS_CODE_ERROR.name().equals(e.getMessage())
//                                || ErrCode.PASS_CODE_INVALID_ERROR.name().equals(e.getMessage())) {
//                            Log.d(TAG, "//---------- onError: SignIn passCode");
//                        } else if (ErrorApi.isGetTokenError(e.getMessage())) {
//                            Log.d(TAG, "//---------- onError: " + App.getInstance().getString(R.string.errorSignIn));
//                        } else {
//                            Log.d(TAG, "//---------- onError: " + e.getMessage());
//                        }
//                        rsTest = false;
//                        dispose();
//                    }
//                });
//        //result
//        assertTrue(rsTest);
    }
    //</editor-fold>

    //<editor-fold desc="Sign Up / Create user">
    @Test
    public void signUp_ReturnSuccess() throws Exception {

        email = "tictacudatest2@gmail.com";

//        Profile profile = new Profile();
//        profile.setFirstName("Tic");
//        profile.setLastName("Tac");
//        profile.setEmail(email);
//        profile.setPhone("1234789123");
//        profile.setSalutation("Dr.");
//        profile.locationToggle(false);
//
//        rsTest = false;
//
//        CreateProfileCase.Params params = mapProfile.createProfile(profile, pwd,
//                String.valueOf(PASS_CODE_VALID)
//        );
//
//        createProfileCase.buildUseCaseCompletable(params)
//                .subscribeWith(new DisposableCompletableObserver(){
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(e.getMessage())){
//                            err += "\n" + context.getString(R.string.errorCreateAccount);
//                        }
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }
    //</editor-fold>

    /**
     * run func signIn: {@link #signIn_CheckPassCode_ReturnSuccess()}
     * to get access from local
     * */
    @Test
    public void updateProfile_ReturnSuccess() throws Exception {
        rsTest = false;
//        loadProfile.loadRemote(getAccessToken)
//                .flatMap(profileCache -> {
//                    //update profile here
//                    profileCache.setPhone("123478");
//
//                    // don't need map profile local
////                    ProfileAspire profileRequest = mapProfile.updateProfile(profileCache, profile);
//
//                    return Single.just(profileCache);
//                })
//                .flatMapCompletable(proUpdated -> saveProfile.updateProfile(proUpdated))
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }

    /**
     * run func signIn: {@link #signIn_CheckPassCode_ReturnSuccess()}
     * to get access from local
     * */
    @Test
    public void changePassword_ReturnError() throws Exception {
        rsTest = false;

        String oldPassword = "Admin@12345";
        String newPassword = "Admin@1234";

        ResetPassword.Params params = new ResetPassword.Params(oldPassword, newPassword);
        changePassword.buildUseCaseCompletable(params)
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }

    /**
     * run func signIn: {@link #signIn_CheckPassCode_ReturnSuccess()}
     * to get access from local
     * */
    @Test
    public void forgotPassword_ReturnError() throws Exception {
        rsTest = false;

        String email = "tictacpcuda100@gmail.com";

//        retrievePassword.param(email)
//                .execute(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });

        //result
        assertTrue(rsTest);
    }

    /**
     * run func signIn: {@link #signIn_CheckPassCode_ReturnSuccess()}
     * to get access from local
     * */
    @Test
    public void updateUserPreference_ReturnSuccess() throws Exception {
        rsTest = false;
//        loadProfile.loadRemote(getAccessToken)
//                .flatMap(profileCache -> {
//                    //update userPreference here
//                    PreferenceData preferenceData = profileCache.getPreferenceData();
//                    preferenceData.setHotel("2 Stars", preferenceData.getHotelPrefID());
//                    profileCache.setPreferenceData(preferenceData);
//
//                    return Single.just(profileCache);
//                })
//                .flatMapCompletable(proUpdated -> saveProfile.updateProfile(proUpdated)
//                        .andThen(loadProfile.loadRemote(getAccessToken)
//                                .flatMapCompletable(proLoadRemote -> Completable.complete())
//                        )
//                )
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }

    @Test
    public void checkPassCode_ReturnSuccess() throws Exception {
        rsTest = false;
        String inputPassCode = "6445785";
//
//        CheckPassCode.Params params = new CheckPassCode.Params(
//                BuildConfig.AS_TOKEN_USERNAME,
//                BuildConfig.AS_TOKEN_SECRET,
//                inputPassCode
//        );
//
//        checkPassCode.buildUseCaseSingle(params)
//                .flatMapCompletable(result -> {
//                    if(result != null && result){
//                        return Completable.complete();
//                    }else{
//                        return Completable.error(new Exception("PassCode Invalid"));
//                    }
//                })
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }


    //<editor-fold desc="Concierge Case">
    @Test
    public void sendRequestConciergeCase_ReturnSuccess() throws Exception {
//        rsTest = false;
//        //value
//        final String content = "Testing";
//        final boolean email = true;
//        final boolean phone = false;
//
//        askConciergeApi.loadStorage()
//                .flatMap(profileAspire -> {
//                    String passCode = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
//                            ConstantAspireApi.APP_USER_PREFERENCES.PASS_CODE_KEY);
//                    int binNumber = CommonUtils.convertStringToInt(passCode);
//                    return Single.just(binNumber);
//                })
//                .flatMap(passCode -> createRequestCase(content, email, phone, passCode))
//                .flatMapCompletable(response -> {
//                    if (response.isEmpty()) {
//                        throw new Exception("Failed to retrieve your profile.");
//                    } else if (response.isSuccess()) {
//                        rsTest = true;
//                    } else {
//                        throw new Exception(response.getMessage());
//                    }
//                    return Completable.complete();
//                })
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }

    private Single<ConciergeCaseAspireResponse> createRequestCase(String content, boolean email, boolean phone, int passCode) {
        return askConciergeApi.execute(new AskConciergeAspire.Params(
                ConciergeCaseType.CREATE,
                content,
                null,//city
                null,
                email,
                phone,
                passCode,
                BuildConfig.AS_PROGRAM,
                AppConstant.CONCIERGE_REQUEST_TYPE.OTHERS.getValue()
        ));
    }
    //</editor-fold>
}