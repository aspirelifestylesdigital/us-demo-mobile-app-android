package com.mastercard.usdemo.presentation.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.MapProfileBase;
import com.api.aspire.domain.usecases.SaveProfile;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.GPSChecker;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.datalayer.repository.B2CDataRepository;
import com.mastercard.usdemo.domain.model.Profile;
import com.mastercard.usdemo.domain.repository.B2CRepository;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;
import com.mastercard.usdemo.presentation.LocationPermissionHandler;
import com.mastercard.usdemo.presentation.base.BaseActivity;
import com.mastercard.usdemo.presentation.checkout.PassCodeCheckout;
import com.mastercard.usdemo.presentation.checkout.PassCodeCheckoutPresenter;
import com.mastercard.usdemo.presentation.checkout.SignInActivity;
import com.mastercard.usdemo.presentation.home.HomeActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class SignUpActivity extends BaseActivity implements ProfileFragment.ProfileEventsListener, CreateProfile.View
        , PassCodeCheckout.View {

    private CreateProfilePresenter presenter;
    private PassCodeCheckoutPresenter passCodeCheckoutPresenter;
    private DialogHelper dialogHelper;
    private View view;
    ProfileFragment fragment;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private CreateProfilePresenter presenter() {
        return new CreateProfilePresenter(this);
    }

    private PassCodeCheckoutPresenter passCodeCheckoutPresenter() {
        return new PassCodeCheckoutPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_fragment_holder);
        view = findViewById(R.id.fragment_place_holder);
        presenter = presenter();
        passCodeCheckoutPresenter = passCodeCheckoutPresenter();
        passCodeCheckoutPresenter.attach(this);
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            fragment = ProfileFragment.newInstance(ProfileFragment.PROFILE.CREATE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder,
                            fragment,
                            ProfileFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showProfileCreatedDialog() {
        dialogHelper.alert(null, getString(R.string.profile_created_message), dialog -> proceedToHome());
    }

    private void proceedToHome() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();

        // Track GA with "Sign up" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (!dialogHelper.networkUnavailability(errCode, extraMsg)) {
            if (ErrorApi.isGetTokenError(extraMsg)) {
                onErrorGetToken();
            } else if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(extraMsg)) {
                dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), App.getInstance().getString(R.string.errorCreateAccount));
            } /*else if (errCode == ErrCode.API_ERROR) {
                dialogHelper.alert("ERROR!", extraMsg);
            } */ else {
                dialogHelper.showGeneralError();
            }
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onCheckPassCodeSuccessfully() {
//        fragment.handleSubmit();
    }

    @Override
    public void onCheckPassCodeFailure() {
        dialogHelper.action(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.ErrorPassCodeEnterValid), getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialogInterface, i) -> invokeError(),
                (dialogInterface, i) -> toSignInScreen());
    }

    @Override
    public void onErrorPassCode() {
        onCheckPassCodeFailure();
    }

    private void invokeError() {
        if (fragment != null) {
            fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
        }
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onProfileCancel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        onBackPressed();
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if (fragment != null) {
                fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
            }
        });
    }

    @Override
    public void onFragmentCreated() {
    }

    @Override
    public void onLocationSwitchOn() {
        if (!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else {
            if (!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                        "Setting", "Cancel",
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (!granted) fragment.switchOff();
        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
            dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
                    "Setting", "Cancel",
                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCheckPassCodeNone() {

    }

    @Override
    public void updatePassCodeApiCompleted() {
        //-- dummy, don't use here
    }

    @Override
    public void onErrorGetToken() {
        if (dialogHelper != null) {
            dialogHelper.showGetTokenError();
        }
    }

    @Override
    public void onSubmitProfile(Profile profile) {

    }

    @Override
    public void onSubmitProfileAndSecurity(Profile profile, String question, String answer) {

    }

    @Override
    public void onSubmitSecurityQuestion(String question, String answer) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
