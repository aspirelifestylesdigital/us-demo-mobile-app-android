package com.mastercard.usdemo.common.constant;

public interface ConciergeCaseType {

    String CREATE = "CREATE";
    String UPDATE = "UPDATE";
    String DELETE = "DELETE";

}
