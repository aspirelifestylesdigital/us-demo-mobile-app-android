package com.mastercard.usdemo.presentation.explore;

import com.mastercard.usdemo.domain.model.LatLng;

/**
 * Created by vinh.trinh on 7/28/2017.
 */

public class DiningSortingCriteria {
    LatLng location;
    String cuisine;
    //    public float distance;
    boolean isRegion;

    public DiningSortingCriteria(LatLng location, String cuisine) {
        this.location = location;
        this.cuisine = cuisine;
    }

    void setLocation(LatLng location) {
        this.location = location;
    }

    void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    void setIsRegion(boolean isRegion) {
        this.isRegion = isRegion;
    }
}