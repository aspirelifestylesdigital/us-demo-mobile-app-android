package com.mastercard.usdemo.presentation.selectcategory;

import android.content.Context;

import com.mastercard.usdemo.presentation.base.BasePresenter;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface Category {

    interface View {
        void proceedWithDiningCategory();
        void askForLocationSetting();
    }

    interface Presenter extends BasePresenter<View> {

        void loadProfile();
        boolean isLocationProfileSettingOn();
        void locationServiceCheck(Context context);
    }
}
