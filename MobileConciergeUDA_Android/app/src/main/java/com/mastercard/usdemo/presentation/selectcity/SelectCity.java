package com.mastercard.usdemo.presentation.selectcity;

import com.mastercard.usdemo.presentation.base.BasePresenter;

/**
 * Created by tung.phan on 5/8/2017.
 */

public interface SelectCity {
    interface View {
        void saveCitySuccessful();
    }

    interface Presenter extends BasePresenter<View> {
        void saveSelectCity(String cityName);
    }
}
