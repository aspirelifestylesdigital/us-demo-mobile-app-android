package com.mastercard.usdemo.common.constant;

/**
 * Created by tung.phan on 5/5/2017.
 */

public interface SharedPrefConstant {

    String PROFILE = "ppfile";
    String SELECTED_CITY = "cityNameSelected";
    String FORGOT_SECRET_KEY = "hasFP";
    String PASSCODE = "passCode";
    String AUTH_TOKEN = "authToken";
}
