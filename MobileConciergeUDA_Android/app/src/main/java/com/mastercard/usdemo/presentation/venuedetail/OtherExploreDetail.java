package com.mastercard.usdemo.presentation.venuedetail;

import com.mastercard.usdemo.domain.model.explore.OtherExploreDetailItem;
import com.mastercard.usdemo.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface OtherExploreDetail {

    interface View {
        void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem);
        void onUpdateFailed();
        void noInternetMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void getContent(Integer contentId);
    }

}
