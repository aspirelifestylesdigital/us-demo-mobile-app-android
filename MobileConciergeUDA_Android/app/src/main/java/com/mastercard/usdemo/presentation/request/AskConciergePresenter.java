package com.mastercard.usdemo.presentation.request;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AspireConciergeRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.usecases.AskConciergeAspire;
import com.api.aspire.domain.usecases.AskRequestStore;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.common.constant.ConciergeCaseType;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class AskConciergePresenter implements CreateNewConciergeCase.Presenter {

    private CompositeDisposable compositeDisposable;
    private CreateNewConciergeCase.View view;
    private AskConciergeAspire askConciergeApi;
    private AskRequestStore askRequestStore;
    private String city;
    private String category;

    AskConciergePresenter(Context context) {
        this.compositeDisposable = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(context);
        this.askConciergeApi = new AskConciergeAspire(mapLogic,
                new AspireConciergeRepository(),
                new GetToken(preferencesStorage, mapLogic),
                new LoadProfile(new ProfileDataAspireRepository(preferencesStorage, mapLogic))
        );
        this.askRequestStore = new AskRequestStore(preferencesStorage);
    }

    @Override
    public void attach(CreateNewConciergeCase.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        this.view = null;
    }

    //<editor-fold desc="Create Request Case">
    @Override
    public void param(String city, String category) {
        this.city = city;
        this.category = category;
    }

    @Override
    public void sendRequest(String content, boolean email, boolean phone) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(
                askConciergeApi.loadStorage()
                        .flatMap(profileAspire -> {
                            String passCode = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);
                            int binNumber = CommonUtils.convertStringToInt(passCode);
                            return Single.just(binNumber);
                        })
                        .flatMap(binCode -> createRequestCase(content, email, phone, binCode))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new AskConciergeObserver())
        );
    }

    private Single<ConciergeCaseAspireResponse> createRequestCase(String content,
                                                                  boolean email, boolean phone,
                                                                  int binCode) {

        return askConciergeApi.execute(new AskConciergeAspire.Params(
                ConciergeCaseType.CREATE,
                content,
                city,
                category,
                email,
                phone,
                binCode,
                BuildConfig.AS_PROGRAM,
                AppConstant.CONCIERGE_REQUEST_TYPE.OTHERS.getValue()));
    }

    private final class AskConciergeObserver extends DisposableSingleObserver<ConciergeCaseAspireResponse> {

        @Override
        public void onSuccess(ConciergeCaseAspireResponse acResponse) {
            view.dismissProgressDialog();
            if (acResponse.isEmpty()) {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, "Failed to retrieve your profile.");
            } else if (acResponse.isSuccess()) {
                view.onRequestSuccessfullySent();
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, acResponse.getMessage());
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }
    //</editor-fold>

    //<editor-fold desc="Save/Load Request Content">
    @Override
    public void saveRequestStorage(String requestContent) {
        askRequestStore.saveContent(requestContent);
    }

    @Override
    public void loadRequestStorage() {

        String requestContent = askRequestStore.loadContent();
        if(!TextUtils.isEmpty(requestContent) && view != null){
            view.loadRequestContentStoreToView(requestContent);
        }

    }
    //</editor-fold>

}
