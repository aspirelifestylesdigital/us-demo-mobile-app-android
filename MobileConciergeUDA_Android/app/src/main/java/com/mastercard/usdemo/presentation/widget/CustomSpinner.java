package com.mastercard.usdemo.presentation.widget;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mastercard.usdemo.R;

/**
 * Created by anh.trinh on 8/8/2017.
 */

public class CustomSpinner {
    private TextView anchorView;
    private ListPopupWindow listPopupWindow;
    private Context context;
    private DropdownAdapter dropdownAdapter;
    private int selectionPosition = -1;

    private int colorDefaultUnSelect = Color.parseColor("#99A1A8");
    private int colorSelected = Color.parseColor("#011627");

    public boolean isNone() {
        return isNone;
    }

    public void setNone(final boolean none) {
        isNone = none;
        this.dropdownAdapter.setNone(isNone);
        this.dropdownAdapter.notifyDataSetChanged();
    }

    boolean isNone = true;

    public int getSelectionPosition() {
        return selectionPosition;
    }

    public int getTextPosition(String text) {
        if (dropdownAdapter == null || dropdownAdapter.getListItems() == null) return -1;

        for (int i = 0; i < dropdownAdapter.getListItems().size(); i++) {
            String question = dropdownAdapter.getListItems().get(i);
            if (question.equalsIgnoreCase(text)) {
                return i;
            }
        }
        return -1;
    }

    public String getTextSelectedSafe() {
        if (dropdownAdapter == null
                || dropdownAdapter.getListItems() == null
                || getSelectionPosition() == -1) {
            return "";
        } else {
            return dropdownAdapter.getListItems().get(getSelectionPosition());
        }
    }

    public void setSelectionPosition(final int selectionPosition) {
        if (selectionPosition == -1) return;

        this.selectionPosition = selectionPosition;
        this.listPopupWindow.setSelection(this.selectionPosition);
        this.dropdownAdapter.setCurrentSelected(selectionPosition);
        if (selectionPosition == 0 && isNone) {
            anchorView.setTextColor(colorDefaultUnSelect);
        } else {
            anchorView.setTextColor(colorSelected);
        }
        String val = dropdownAdapter.getItem(selectionPosition);
        val = TextUtils.isEmpty(val) ? "" : val;
        anchorView.setText(val);
    }

    public interface SpinnerListener {
        void onArchorViewClick();

        void onItemSelected(int index);
    }

    public SpinnerListener getmSpinnerListener() {
        return mSpinnerListener;
    }

    public void setmSpinnerListener(final SpinnerListener mSpinnerListener) {
        this.mSpinnerListener = mSpinnerListener;
    }

    SpinnerListener mSpinnerListener;

    public CustomSpinner(Context context,
                         final TextView anchorView, boolean isNone) {
        this.context = context;
        this.anchorView = anchorView;
        this.isNone = isNone;
        listPopupWindow = new ListPopupWindow(anchorView.getContext());
        anchorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onArchorViewClick();
                }
                if(listPopupWindow.isShowing()){
                    listPopupWindow.dismiss();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
                }else {
                    listPopupWindow.show();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_show);
                }
            }
        });

        listPopupWindow.setAnchorView(anchorView);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
            }
        });
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onItemSelected(i);
                }
                if (i == 0 &&isNone) {
                    anchorView.setTextColor(colorDefaultUnSelect);
                } else {
                    anchorView.setTextColor(colorSelected);
                }
                anchorView.setText(adapterView.getItemAtPosition(i)
                                              .toString());
                selectionPosition = i;

                dropdownAdapter.setCurrentSelected(i);
                listPopupWindow.dismiss();

            }
        });
    }
    public CustomSpinner(Context context,
                         final TextView anchorView) {
        this.context = context;
        this.anchorView = anchorView;
        listPopupWindow = new ListPopupWindow(anchorView.getContext());
        anchorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onArchorViewClick();
                }
                if(listPopupWindow.isShowing()){
                    listPopupWindow.dismiss();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
                }else {
                    listPopupWindow.show();
                    anchorView.setBackgroundResource(R.drawable.bg_spinner_show);
                }
            }
        });
        listPopupWindow.setModal(true);
        listPopupWindow.setAnchorView(anchorView);
        listPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
            }
        });
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view,
                                    final int i,
                                    final long l) {
                if (mSpinnerListener != null) {
                    mSpinnerListener.onItemSelected(i);
                }
                if (i == 0 &&isNone) {
                    anchorView.setTextColor(colorDefaultUnSelect);
                } else {
                    anchorView.setTextColor(colorSelected);
                }
                anchorView.setText(adapterView.getItemAtPosition(i)
                                              .toString());
                selectionPosition = i;
                dropdownAdapter.setCurrentSelected(i);
                listPopupWindow.dismiss();

            }
        });
    }

    public static int convertDpToPx(int dp,
                                    Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                               dp,
                                               context.getResources()
                                                      .getDisplayMetrics());
    }

    public void setPobpupHeight(int popupHeight) {
        listPopupWindow.setHeight(convertDpToPx(popupHeight,
                                                context));
    }

    public void setPopupHeight(int minHeight) {
        if(dropdownAdapter != null
                && dropdownAdapter.getListItems() != null){
            int dataSize = dropdownAdapter.getListItems().size();
            int maxHeight = dataSize * convertDpToPx(minHeight, context);
            listPopupWindow.setHeight(maxHeight);
        }
    }

    public void setPopupHeightListQuestion() {
        if(dropdownAdapter != null
                && dropdownAdapter.getListItems() != null){
            int dataSize = dropdownAdapter.getListItems().size();
            int maxHeight = dataSize * convertDpToPx(52, context);
            if (maxHeight < 245) {
                listPopupWindow.setHeight(maxHeight);
            } else {
                listPopupWindow.setHeight(convertDpToPx(245, context));
            }
        }
    }


    public void setDefaultUnSelect(String textHint){
        anchorView.setText(textHint);
        anchorView.setTextColor(colorDefaultUnSelect);
    }

    public DropdownAdapter getDropdownAdapter() {
        return dropdownAdapter;
    }

    public void setDropdownAdapter(final DropdownAdapter dropdownAdapter) {
        this.dropdownAdapter = dropdownAdapter;
        this.dropdownAdapter.setNone(isNone);
        listPopupWindow.setAdapter(dropdownAdapter);
    }

    public void setCurrentSelected(){
        this.dropdownAdapter.setCurrentSelected(-1);
    }

}
