package com.mastercard.usdemo.presentation.selectcategory;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.CityData;
import com.mastercard.usdemo.common.glide.GlideHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

//    private final int CITY_GUIDE_POSITION = 13; // city guide item position in category rview
    private final int expectedImageWidth;
    private final int expectedImageLargeWidth;
    private final int expectedItemHeight;
    private TypedArray images;
    private String[] categoryList;
    private OnCategoryItemClickListener listener;
    private int colorWhite, colorAccent, colorBlack, semiWhite;
    private Context context;

    CategoryAdapter(Context context) {
        this.context = context;
        Resources resources = context.getResources();
        if (isCityGuideSupported()){
            categoryList = resources.getStringArray(R.array.category_texts);
            images = resources.obtainTypedArray(R.array.category_images);
        }else{
            categoryList = resources.getStringArray(R.array.category_texts_without_city_guide);
            images = resources.obtainTypedArray(R.array.category_images_without_city_guide);
        }


        colorWhite = ContextCompat.getColor(context, R.color.white);
        colorAccent = ContextCompat.getColor(context, R.color.brand_red);
        colorBlack = ContextCompat.getColor(context, R.color.colorPrimary);
        semiWhite = ContextCompat.getColor(context, R.color.semi_white);
        expectedImageWidth = (int) (App.getInstance().getResources().getDisplayMetrics().widthPixels -
                resources.getDimension(R.dimen.padding_medium) -
                resources.getDimension(R.dimen.padding_medium) -
                resources.getDimension(R.dimen.grid_item_margin) -
                resources.getDimension(R.dimen.grid_item_margin)
        ) / 2;

        expectedImageLargeWidth = (int) (App.getInstance().getResources().getDisplayMetrics().widthPixels -
                resources.getDimension(R.dimen.padding_medium) -
                resources.getDimension(R.dimen.padding_medium));

        expectedItemHeight = (int) (App.getInstance().getResources().getDisplayMetrics().heightPixels) / (11 / 2);

    }

    public String[] getCategoryList() {
        return this.categoryList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        boolean clickable = true;
        //if (viewType == 13 && !isCityGuideSupported()) clickable = false;
        return new CategoryViewHolder(
                ((LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                        .inflate(R.layout.category_item, parent, false), clickable);
    }

    /**
     * @return true if currentSelectedCity is one of the city in city_guide_list
     */
    private boolean isCityGuideSupported() {
        return CityData.guideCode() > 0;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        /*if(position == 2) {
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP);
        } else {
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP);
        }*/
        holder.text.setText(categoryList[position]);
//        if (holder.text.getText().toString().equals("TRANSPORTATION")){
//            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.font_size_smallest));
//        }

        if (position == 0) {
            holder.itemView.getLayoutParams().width = expectedImageLargeWidth;
        } else {
            holder.itemView.getLayoutParams().width = expectedImageWidth;
        }

//        Picasso.with(context)
//                .load(images.getResourceId(position, R.drawable.img_placeholder))
//                .fit()
//                .into(holder.image);
        GlideHelper.getInstance().loadImage(images.getResourceId(position, R.drawable.img_placeholder),
                0, holder.image, expectedImageWidth);
    }

    @Override
    public int getItemCount() {
        return categoryList.length;
    }

    void setListener(OnCategoryItemClickListener listener) {
        this.listener = listener;
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {

        @BindView(R.id.category_image)
        AppCompatImageView image;
        @BindView(R.id.mask)
        AppCompatImageView mask;
        @BindView(R.id.category_text)
        AppCompatTextView text;

        CategoryViewHolder(View itemView, boolean clickable) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnTouchListener(this);
            itemView.setVisibility(View.VISIBLE);
            itemView.getLayoutParams().width = expectedImageWidth;
            itemView.getLayoutParams().height = expectedItemHeight;

//            if (clickable) {
//
//            } else {
//                itemView.setVisibility(View.GONE);
//                itemView.getLayoutParams().width = expectedImageWidth;
//                itemView.getLayoutParams().height = 0;
//            }

//            } else {
//                mask.setBackgroundColor(semiWhite);
//                mask.setAlpha(0.8f);
//            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.performClick();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mask.setAlpha(0.2f);
                    mask.setBackgroundColor(colorWhite);
                    text.setTextColor(colorAccent);
                    text.setShadowLayer(0, 0, 0, 0);
                    break;
                case MotionEvent.ACTION_UP:
                    if (listener != null) {
                        listener.onItemClick(text.getText().toString());
                    }
                    mask.setAlpha(0.2f);
                    mask.setBackgroundColor(colorBlack);
                    text.setTextColor(colorWhite);
                    text.setShadowLayer(context.getResources().getInteger(R.integer.shadow_radius), 0, context.getResources().getInteger(R.integer.shadow_dy), colorBlack);
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                default:
                    text.setTextColor(colorWhite);
                    mask.setAlpha(0.2f);
                    mask.setBackgroundColor(colorBlack);
                    text.setShadowLayer(context.getResources().getInteger(R.integer.shadow_radius), 0, context.getResources().getInteger(R.integer.shadow_dy), colorBlack);
                    break;
            }
            return true;
        }
    }

    public interface OnCategoryItemClickListener {
        void onItemClick(String category);
    }
}
