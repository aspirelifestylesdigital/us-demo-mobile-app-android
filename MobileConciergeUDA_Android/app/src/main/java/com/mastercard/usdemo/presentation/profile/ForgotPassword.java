package com.mastercard.usdemo.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.usdemo.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public interface ForgotPassword {
    interface View {
        void showProgressDialog();
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode, String extraMsg);
        void showSuccessMessage();
    }

    interface Presenter extends BasePresenter<View> {
        void retrievePassword(String email);
    }
}
