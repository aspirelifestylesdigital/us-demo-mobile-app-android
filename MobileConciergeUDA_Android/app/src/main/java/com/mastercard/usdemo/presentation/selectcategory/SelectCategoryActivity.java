package com.mastercard.usdemo.presentation.selectcategory;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.api.aspire.domain.usecases.LoadProfile;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.common.constant.CityData;
import com.mastercard.usdemo.common.constant.IntentConstant;
import com.mastercard.usdemo.common.constant.RequestCode;
import com.mastercard.usdemo.common.constant.ResultCode;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;
import com.mastercard.usdemo.presentation.LocationPermissionHandler;
import com.mastercard.usdemo.presentation.base.CommonActivity;
import com.mastercard.usdemo.presentation.cityguidecategory.CityGuideCategoriesActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class SelectCategoryActivity extends CommonActivity implements
        CategoryAdapter.OnCategoryItemClickListener
        , Category.View {

    @BindView(R.id.selection_recycle_view)
    RecyclerView categoriesListView;
    private CategoryAdapter adapter;
    private DialogHelper dialogHelper;
    private CategoryPresenter presenter;
    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private CategoryPresenter presenter() {
        return new CategoryPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        setTitle(R.string.category_title);
        setToolbarColor(R.color.colorPrimaryDark);
        presenter = presenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        categoriesListView.setLayoutManager(manager);
        categoriesListView.addItemDecoration(
                new SelectCategorySpacesItemDecoration(
                        getResources().getDimensionPixelSize(R.dimen.grid_item_margin)));
        adapter = new CategoryAdapter(this);
        adapter.setListener(this);
        categoriesListView.setAdapter(adapter);
        presenter.loadProfile();

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 2;
                }
                return 1;
            }
        });

        categoriesListView.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_smallest));

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CATEGORY_LIST.getValue());
        }
    }

    @OnClick(R.id.btn_all_category)
    void allCategoryBtnClick() {
        returnResult("");
    }

    @Override
    public void onItemClick(String category) {
        if (adapter.getCategoryList().length > 13 && category.equalsIgnoreCase(adapter.getCategoryList()[13])) {//click on City Guide
            //return the category id of the current city selected if that city support city guide
            int cityId = CityData.guideCode();
            //start sub category select
            Intent intent = new Intent(this, CityGuideCategoriesActivity.class);
            intent.putExtra(IntentConstant.CATEGORY_ID, cityId);
            startActivityForResult(intent, RequestCode.SELECT_SUB_CATEGORY);
        } else {
            if (category.equals("DINING")) {
                if (presenter.isLocationProfileSettingOn()) {
                    if (!locationPermissionHandler.hasPermission(this)) {
                        locationPermissionHandler.requestPermission(this);
                    } else {
                        presenter.locationServiceCheck(getApplicationContext());
                    }
                } else {
                    returnResult(category);
                }
            } else {
                returnResult(category);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
        if (granted) {
            presenter.locationServiceCheck(getApplicationContext());
        } else {
            proceedWithDiningCategory();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            if (resultCode == ResultCode.RESULT_OK && null != data) {
                data.putExtra(IntentConstant.SUPPER_CATEGORY
                        , getResources().getString(R.string.city_guide));
                setResult(ResultCode.RESULT_OK_WITH_ID, data);
                finish();
            }
        }
        if (requestCode == 11234) {
            returnResult("DINING");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public void proceedWithDiningCategory() {
        returnResult("DINING");
    }

    @Override
    public void askForLocationSetting() {
        dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.", "Setting", "Cancel",
                (dialogInterface, i) -> openGPSSetting(),
                (dialogInterface, i) -> returnResult("DINING"));
    }

    private void returnResult(String category) {
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.SELECTED_CATEGORY, category);
        setResult(ResultCode.RESULT_OK, intent);
        finish();
    }

    private void openGPSSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, 11234);
    }
}
