package com.mastercard.usdemo.domain.usecases;

import android.content.Context;
import android.text.TextUtils;
import com.mastercard.usdemo.common.constant.CityData;
import com.mastercard.usdemo.datalayer.datasource.AppGeoCoder;
import com.mastercard.usdemo.datalayer.entity.Answer;
import com.mastercard.usdemo.datalayer.entity.QuestionsAndAnswer;
import com.mastercard.usdemo.domain.model.LatLng;
import com.mastercard.usdemo.domain.model.explore.DiningDetailItem;
import com.mastercard.usdemo.domain.model.explore.ExploreRView;
import com.mastercard.usdemo.domain.model.explore.ExploreRViewItem;
import com.mastercard.usdemo.domain.repository.B2CRepository;
import com.mastercard.usdemo.presentation.explore.DiningSortingCriteria;
import com.mastercard.usdemo.presentation.explore.ExplorePresenter;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public class GetDinningList extends UseCase<List<ExploreRViewItem>, GetDinningList.Params> {
    private B2CRepository b2CRepository;
    private AppGeoCoder geoCoder;
    private List<QuestionsAndAnswer> data;
    private LatLng location;
    private Context context;

    public GetDinningList(B2CRepository b2CRepository, AppGeoCoder geoCoder) {
        super();
        this.b2CRepository = b2CRepository;
        this.geoCoder = geoCoder;
        data = new ArrayList<>();
    }

    @Override
    Observable<List<ExploreRViewItem>> buildUseCaseObservable(GetDinningList.Params params) {
        return null;
    }

    @Override
    public Single<List<ExploreRViewItem>> buildUseCaseSingle(GetDinningList.Params params) {
        if (params.paging == ExplorePresenter.DEFAULT_PAGE) data.clear();

        int length = data.size();
        return b2CRepository.getDiningList(params.condition, params.paging)
                .map(questionsAndAnswers -> {
                    data.addAll(questionsAndAnswers);
                    return questionsAndAnswers;
                })
                .flatMap(questionsAndAnswers -> {
                    return Single.zip(viewData(length, questionsAndAnswers), objects -> {
                        List<ExploreRViewItem> exploreRViewItems = new ArrayList<>();
                        for (int i = 0; i < objects.length; i++) {
                            exploreRViewItems.add((ExploreRViewItem) objects[i]);
                        }
                        return exploreRViewItems;
                    });
                })
                .flatMap(exploreRViewItems -> {
                    return Single.just(exploreRViewItems);
                });


    }

    /**
     * @param startIndex
     * @param data
     * @return
     */
    private List<Single<ExploreRViewItem>> viewData(int startIndex, List<QuestionsAndAnswer> data) {
        List<Single<ExploreRViewItem>> singles = new ArrayList<>();
        int length = data.size();
//            for (int i = 0; i < length; i++) {
//                QuestionsAndAnswer qa = data.get(i);
//                final ExploreRViewItem exploreRViewItem = viewDatum(startIndex + i, qa);
//                exploreRViewList.add(exploreRViewItem);
//            }

        for (int i = 0; i < length; i++) {
            QuestionsAndAnswer qa = data.get(i);
            singles.add(createSingleExploreRViewItem(startIndex + i, qa).subscribeOn(Schedulers.newThread()));
        }
        return singles;
    }

    private Single<ExploreRViewItem> createSingleExploreRViewItem(int startIndex, QuestionsAndAnswer qa) {
        return Single.fromCallable(() -> {
            final ExploreRViewItem exploreRViewItem = viewDatum(startIndex, qa);
            return exploreRViewItem;
        });
    }

    private ExploreRViewItem viewDatum(int dataIndex, QuestionsAndAnswer questionsAndAnswers) {
        Answer answer = questionsAndAnswers.getAnswers().get(0);
        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
                questionsAndAnswers.getID(),
                answer.getName().trim(),
                answer.getCity() + ", " + (CityData.isUSCity() ? answer.getState() : answer.getCountry()),
                answer.getImageURL(),
                !TextUtils.isEmpty(answer.getOffer2()),
                answer.getOffer2(),
                dataIndex
        );
        exploreRViewItem.setItemType(ExploreRViewItem.ItemType.DINING);
        exploreRViewItem.categoryName("dining");
        String desireAddress = desireAddress(answer);
        LatLng latLng = geoCoder.getLocation(desireAddress,location,context);
        /*Log.d("vinhtv", answer.getName());
        Log.d("vinhtv", desireAddress);
        Log.d("vinhtv", latLng.toString());*/
        exploreRViewItem.setSortingCriteria(new DiningSortingCriteria(
                latLng,
                answer.getUserDefined1()
        ));
        return exploreRViewItem;
    }
    public DiningDetailItem getItemView(int index) {
        QuestionsAndAnswer datum = data.get(index);
        Answer answer = datum.getAnswers().get(0);
        //LatLng latLng = geoCoder.getLocation(answer.getAddress());
        DiningDetailItem item = new DiningDetailItem.Builder(
                answer.getName(),
                answer.getAddress(),
                answer.getCity(),
                answer.getZipCode(),
                answer.getPrice(),
                answer.getUserDefined1(),
                answer.getAnswerText(),
                answer.getHoursOfOperation(),
                answer.getImageURL())
                .address3(answer.getAddress3())
                .benefits(answer.getOffer2())
                .state(answer.getState())
                .url(answer.getURL())
                .coordination(0, 0)
                .build();
        item.setItemType(ExploreRView.ItemType.DINING);
        return item;
    }

    private String desireAddress(Answer answer) {
        return answer.getAddress() +
                (TextUtils.isEmpty(answer.getCity().trim()) ? "" : ", " + answer.getCity()) +
                (TextUtils.isEmpty(answer.getState().trim()) ? "" : ", " + answer.getState()) +
                (TextUtils.isEmpty(answer.getZipCode().trim()) ? "" : " " + answer.getZipCode()) +
                (TextUtils.isEmpty(answer.getCountry().trim()) ? "" : ", " + answer.getCountry());
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public void setLocation(LatLng location, Context context) {
        this.location = location;
        this.context = context;
    }

    public static final class Params {

        private final Integer condition;
        private final Integer paging;

        public Params(Integer categoryId) {
            this.condition = categoryId;
            paging = 0;
        }

        public Params(Integer categoryID, Integer paging) {
            this.condition = categoryID;
            this.paging = paging;
        }
    }
}
