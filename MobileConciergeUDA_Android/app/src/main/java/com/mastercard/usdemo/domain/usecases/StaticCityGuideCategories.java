package com.mastercard.usdemo.domain.usecases;

import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.CityData;
import com.mastercard.usdemo.common.constant.CityGuide;
import com.mastercard.usdemo.common.constant.SubCategory;
import com.mastercard.usdemo.domain.model.SubCategoryItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 6/7/2017.
 */

public class StaticCityGuideCategories extends UseCase<List<SubCategoryItem>, StaticCityGuideCategories.Params> {

    @Override
    Observable<List<SubCategoryItem>> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<List<SubCategoryItem>> buildUseCaseSingle(Params params) {
        CityGuide cityGuide = CityData.cityGuide(params.categoryId);
        return Single.just(provideData(cityGuide));
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    private List<SubCategoryItem> provideData(CityGuide cityGuide) {
        List<SubCategoryItem> subCategoryItems = new ArrayList<>();
        /*if(cityGuide.size() > 1) {
            subCategoryItems.add(item(cityGuide, CityGuide.ACCOMMODATION_LIST_INDEX));
            subCategoryItems.add(item(cityGuide, CityGuide.BAR_LIST_INDEX));
            subCategoryItems.add(item(cityGuide, CityGuide.CULTURE_LIST_INDEX));
            subCategoryItems.add(item(cityGuide, CityGuide.DINNING_LIST_INDEX));
            subCategoryItems.add(item(cityGuide, CityGuide.SHOPPING_LIST_INDEX));
            subCategoryItems.add(item(cityGuide, CityGuide.SPA_LIST_INDEX));
        } else {
            subCategoryItems.add(item(cityGuide, CityGuide.ACCOMMODATION_LIST_INDEX));
        }*/
        subCategoryItems.add(item(cityGuide, CityGuide.BAR_LIST_INDEX));
        subCategoryItems.add(item(cityGuide, CityGuide.CULTURE_LIST_INDEX));
        subCategoryItems.add(item(cityGuide, CityGuide.DINNING_LIST_INDEX));
        subCategoryItems.add(item(cityGuide, CityGuide.SHOPPING_LIST_INDEX));
        subCategoryItems.add(item(cityGuide, CityGuide.SPA_LIST_INDEX));
        return subCategoryItems;
    }

    private SubCategoryItem item(CityGuide cityGuide, int index) {
        int image, id;
        String title;
        switch (index) {
            /*case CityGuide.ACCOMMODATION_LIST_INDEX:
                image = R.drawable.sub_category_accomodation;
                title = SubCategory.ACCOMMODATIONS;
                id = cityGuide.accommodation;
                break;*/
            case CityGuide.BAR_LIST_INDEX:
                image = R.drawable.sub_category_bar_club;
                title = SubCategory.BARS;
                id = cityGuide.bar;
                break;
            case CityGuide.CULTURE_LIST_INDEX:
                image = R.drawable.sub_category_culture;
                title = SubCategory.CULTURE;
                id = cityGuide.culture;
                break;
            case CityGuide.DINNING_LIST_INDEX:
                image = R.drawable.sub_category_dining;
                title = SubCategory.DINING;
                id = cityGuide.dining;
                break;
            case CityGuide.SHOPPING_LIST_INDEX:
                image = R.drawable.sub_category_shopping;
                title = SubCategory.SHOPPING;
                id = cityGuide.shopping;
                break;
            case CityGuide.SPA_LIST_INDEX:
                image = R.drawable.sub_category_spas;
                title = SubCategory.SPAS;
                id = cityGuide.spa;
                break;
            default:
                return null;
        }
        SubCategoryItem subCategoryItem = new SubCategoryItem();
        subCategoryItem.setImageResources(image);
        subCategoryItem.setSubCategoryName(title);
        subCategoryItem.setId(id);
        return subCategoryItem;
    }

    public static final class Params {

        private final Integer categoryId;

        public Params(Integer categoryId) {
            this.categoryId = categoryId;
        }
    }
}
