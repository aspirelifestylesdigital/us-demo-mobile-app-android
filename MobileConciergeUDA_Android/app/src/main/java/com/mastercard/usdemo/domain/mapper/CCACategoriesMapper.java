package com.mastercard.usdemo.domain.mapper;

import android.text.TextUtils;
import android.util.SparseArray;

import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinh.trinh on 7/25/2017.
 */

public class CCACategoriesMapper {

    public enum WHICH {NAME, ID}

    private final SparseArray<String> categoryNameMapper;
    private final Map<String, List<Integer>> categoryIDMapper;
    private final List<Integer> ccaMultipleCities = Arrays.asList(16,
            31,
            976,
            1035,
            28,
            24,
            314,
            315,
            316,
            1027,
            1251,
            13,
            17,
            23,
            931,
            1037,
            1038,
            1246,
            1250,
            27,
            15,
            934,
            14,
            1247,
            1037,
            1038
    );

    public CCACategoriesMapper() {
        categoryNameMapper = null;
        categoryIDMapper = null;
    }

    public CCACategoriesMapper(WHICH wh) {
        if(wh == WHICH.NAME) {
            categoryNameMapper = new SparseArray<>();
            categoryNameMapper.put(934,"Flowers");
            categoryNameMapper.put(15,"Flowers");

            categoryNameMapper.put(28,"Entertainment");
            categoryNameMapper.put(1137,"Entertainment");
            categoryNameMapper.put(1023,"Entertainment");
            categoryNameMapper.put(1138,"Entertainment");
            categoryNameMapper.put(1139,"Entertainment");
            categoryNameMapper.put(1244,"Entertainment");
            categoryNameMapper.put(1245,"Entertainment");

            categoryNameMapper.put(16,"Golf");
            categoryNameMapper.put(27,"Golf");
            categoryNameMapper.put(31,"Golf");
            categoryNameMapper.put(976,"Golf");
            categoryNameMapper.put(1237,"Golf");
            categoryNameMapper.put(1238,"Golf");
            categoryNameMapper.put(1239,"Golf");
            categoryNameMapper.put(1240,"Golf");

            categoryNameMapper.put(35,"Vacation Packages");
            categoryNameMapper.put(1255,"Vacation Packages");
            categoryNameMapper.put(1256,"Vacation Packages");
            categoryNameMapper.put(1257,"Vacation Packages");
            categoryNameMapper.put(1258,"Vacation Packages");
            categoryNameMapper.put(1259,"Vacation Packages");
            categoryNameMapper.put(1260,"Vacation Packages");
            categoryNameMapper.put(1261,"Vacation Packages");
            categoryNameMapper.put(1262,"Vacation Packages");
            categoryNameMapper.put(1263,"Vacation Packages");
            categoryNameMapper.put(1264,"Vacation Packages");
            categoryNameMapper.put(1265,"Vacation Packages");
            categoryNameMapper.put(1266,"Vacation Packages");
            categoryNameMapper.put(1267,"Vacation Packages");
            categoryNameMapper.put(1269,"Vacation Packages");
            categoryNameMapper.put(1270,"Vacation Packages");
            categoryNameMapper.put(1271,"Vacation Packages");
            categoryNameMapper.put(1274,"Vacation Packages");
            categoryNameMapper.put(1278,"Vacation Packages");
            categoryNameMapper.put(1279,"Vacation Packages");
            categoryNameMapper.put(1280,"Vacation Packages");
            categoryNameMapper.put(1281,"Vacation Packages");
            categoryNameMapper.put(1282,"Vacation Packages");
            categoryNameMapper.put(1284,"Vacation Packages");
            categoryNameMapper.put(1285,"Vacation Packages");
            categoryNameMapper.put(1286,"Vacation Packages");
            categoryNameMapper.put(1287,"Vacation Packages");
            categoryNameMapper.put(1302,"Vacation Packages");
            categoryNameMapper.put(1303,"Vacation Packages");
            categoryNameMapper.put(1304,"Vacation Packages");
            categoryNameMapper.put(1305,"Vacation Packages");
            categoryNameMapper.put(1306,"Vacation Packages");
            categoryNameMapper.put(1307,"Vacation Packages");
            categoryNameMapper.put(1308,"Vacation Packages");
            categoryNameMapper.put(1310,"Vacation Packages");
            categoryNameMapper.put(1312,"Vacation Packages");
            categoryNameMapper.put(1313,"Vacation Packages");
            categoryNameMapper.put(1314,"Vacation Packages");
            categoryNameMapper.put(1315,"Vacation Packages");
            categoryNameMapper.put(1316,"Vacation Packages");
            categoryNameMapper.put(1317,"Vacation Packages");
            categoryNameMapper.put(1318,"Vacation Packages");
            categoryNameMapper.put(1319,"Vacation Packages");
            categoryNameMapper.put(1320,"Vacation Packages");
            categoryNameMapper.put(1321,"Vacation Packages");
            categoryNameMapper.put(1322,"Vacation Packages");
            categoryNameMapper.put(1323,"Vacation Packages");

            categoryNameMapper.put(25,"Cruise");
            categoryNameMapper.put(1067,"Cruise");
            categoryNameMapper.put(1070,"Cruise");
            categoryNameMapper.put(1071,"Cruise");
            categoryNameMapper.put(1077,"Cruise");
            categoryNameMapper.put(1081,"Cruise");
            categoryNameMapper.put(1115,"Cruise");
            categoryNameMapper.put(1116,"Cruise");
            categoryNameMapper.put(1117,"Cruise");
            categoryNameMapper.put(1325,"Cruise");
            categoryNameMapper.put(1326,"Cruise");
            categoryNameMapper.put(1327,"Cruise");
            categoryNameMapper.put(1328,"Cruise");
            categoryNameMapper.put(1329,"Cruise");
            categoryNameMapper.put(1330,"Cruise");
            categoryNameMapper.put(1331,"Cruise");
            categoryNameMapper.put(1332,"Cruise");
            categoryNameMapper.put(1333,"Cruise");
            //Updata Data
            categoryNameMapper.put(1086,"Cruise");
            categoryNameMapper.put(1370,"Cruise");
            categoryNameMapper.put(1371,"Cruise");
            categoryNameMapper.put(1372,"Cruise");
            categoryNameMapper.put(1373,"Cruise");
            categoryNameMapper.put(1374,"Cruise");
            categoryNameMapper.put(1375,"Cruise");


            categoryNameMapper.put(17,"Tours");
            categoryNameMapper.put(931,"Tours");

            categoryNameMapper.put(1035,"Travel");
            categoryNameMapper.put(36,"Travel");
            categoryNameMapper.put(1137,"Travel");

            categoryNameMapper.put(179,"Dining");
            categoryNameMapper.put(259,"Dining");
            categoryNameMapper.put(261,"Dining");
            categoryNameMapper.put(263,"Dining");
            categoryNameMapper.put(264,"Dining");
            categoryNameMapper.put(1241,"Dining");
            // Update Data
            categoryNameMapper.put(1341,"Dining");

            categoryNameMapper.put(1037,"Airport Services");
            categoryNameMapper.put(1038,"Airport Services");

            categoryNameMapper.put(13,"Travel Services");
            categoryNameMapper.put(23,"Travel Services");
            categoryNameMapper.put(1246,"Travel Services");
            categoryNameMapper.put(1250,"Travel Services");

            categoryNameMapper.put(24,"Transportation");
            categoryNameMapper.put(314,"Transportation");
            categoryNameMapper.put(315,"Transportation");
            categoryNameMapper.put(316,"Transportation");
            categoryNameMapper.put(1027,"Transportation");
            categoryNameMapper.put(1251,"Transportation");

            categoryNameMapper.put(14,"Shopping");
            categoryNameMapper.put(15,"Shopping");
            categoryNameMapper.put(610,"Shopping");
            categoryNameMapper.put(1247,"Shopping");
        } else {
            categoryNameMapper = null;
        }
        if(wh == WHICH.ID) {
            categoryIDMapper = new HashMap<>();
            categoryIDMapper.put("entertainment", Arrays.asList(28,1137,1023,1138,1139,1244,1245));
            categoryIDMapper.put("golf", Arrays.asList(16,27,31,976,1237,1238,1239,1240));
            categoryIDMapper.put("tours", Arrays.asList(931,17));
            categoryIDMapper.put("airport services", Arrays.asList(1037,1038));
            categoryIDMapper.put("travel services", Arrays.asList(13,23,1246,1250));
            categoryIDMapper.put("shopping", Arrays.asList(14,16,15,27,31,610,934,976,1247));
            // Update Data
            //categoryIDMapper.put("dining", Arrays.asList(179,259,261,263,264,1241));
            categoryIDMapper.put("dining", Arrays.asList(179,259,261,263,264,1241,1341));
            categoryIDMapper.put("cruise", Arrays.asList( 25,1067,1070,1071,1077,1081,1115,1116,1117,1325,1326,1327,1328,1329,1330,1331,
            1332,1333,1086,1370,1371,1372,1373,1374,1375));
        } else {
            categoryIDMapper = null;
        }
    }

    public String categoryName(Integer id) {
        return categoryNameMapper.get(id);
    }

    public List<Integer> ids(String category) {
        return categoryIDMapper.get(category);
    }

    public boolean shouldFilter(String category) {
        return categoryIDMapper.keySet().contains(category);
    }

    /** return text value */
    public String multipleCitiesText(String categoryName, String subCategoryName) {
        return this.multipleCities(categoryName, subCategoryName) ? App.getInstance().getString(R.string.text_multiple_cities) : " ";
    }

    /** default return false
     * categoryName will show multiple cities:
     * Airport Clubs, Flowers, Golf, Golf Merchandise, Private Jet Travel,
     * Shopping, Tickets, Transportation, VIP Travel Services
     *
     * don't show:
     * Cruises, Hotels, Retail Shopping, Specialty Travel, Vacation Packages
     *
     * Dining and City Guides should never list multiple cities
     * */
    public boolean multipleCities(String categoryName, String subCategoryName) {
        boolean rs;
        String categoryCode = getCategoryCode(categoryName, subCategoryName);
        if (TextUtils.isEmpty(categoryCode)
                || AppConstant.EXPLORE_CATEGORY.CRUISE_APP.getValue().equalsIgnoreCase(categoryCode)
                || AppConstant.EXPLORE_CATEGORY.HOTEL.getValue().equalsIgnoreCase(categoryCode)
                || AppConstant.EXPLORE_CATEGORY.RETAIL_SHOPPING.getValue().equalsIgnoreCase(categoryCode)
                || AppConstant.EXPLORE_CATEGORY.SPECIALTY_TRAVEL.getValue().equalsIgnoreCase(categoryCode)
                || AppConstant.EXPLORE_CATEGORY.VACATION_PACKAGES.getValue().equalsIgnoreCase(categoryCode)
                || AppConstant.EXPLORE_CATEGORY.DINING.getValue().equalsIgnoreCase(categoryCode) ) {
            rs = false;
        } else {
            rs = true;
        }
        return rs;
    }

    /**
     * default return categoryName, otherwise
     * return follow map logic category and subCategory
     */
    public static String getCategoryCode(String categoryName, String subCategoryName) {

        if ("Dining".equalsIgnoreCase(categoryName) ||
                (("Specialty Travel".equalsIgnoreCase(categoryName))) && ("Culinary Experiences".equalsIgnoreCase(subCategoryName))) {
            return "Dining";
        }

        if ("Hotels".equalsIgnoreCase(categoryName)) {
            return "Hotels";
        }

        if ("Flowers".equalsIgnoreCase(categoryName)) {
            return "Flowers";
        }

        if ("Tickets".equalsIgnoreCase(categoryName) ||
                (("Specialty Travel".equalsIgnoreCase(categoryName))) &&
                        (("Entertainment Experiences".equalsIgnoreCase(subCategoryName)) || ("Major Sports Events".equalsIgnoreCase(subCategoryName)))) {
            return "Entertainment";
        }

        if ("Golf".equalsIgnoreCase(categoryName) || "Golf Merchandise".equalsIgnoreCase(categoryName)
                || (("Specialty Travel".equalsIgnoreCase(categoryName))) && ("Golf Experiences".equalsIgnoreCase(subCategoryName))) {
            return "Golf";
        }

        if ("Cruises".equalsIgnoreCase(categoryName)) {
            return AppConstant.EXPLORE_CATEGORY.CRUISE_APP.getValue();
        }

        if ("VIP Travel Services".equalsIgnoreCase(categoryName) && "Sightseeing".equalsIgnoreCase(subCategoryName)) {
            return "Tours";
        }

        if ("Private Jet Travel".equalsIgnoreCase(categoryName)) {
            return "Travel";
        }

        if ("VIP Travel Services".equalsIgnoreCase(categoryName) && "Airport Services".equalsIgnoreCase(subCategoryName)) {
            return "Airport Services";
        }

        if ("VIP Travel Services".equalsIgnoreCase(categoryName) && "Travel Services".equalsIgnoreCase(subCategoryName)) {
            return "Travel Services";
        }

        if ("Transportation".equalsIgnoreCase(categoryName)) {
            return "Transportation";
        }

        if ("Golf Merchandise".equalsIgnoreCase(categoryName) ||
                "Flowers".equalsIgnoreCase(categoryName) || "Wine".equalsIgnoreCase(categoryName) ||
                "Retail Shopping".equalsIgnoreCase(categoryName) || "Shopping".equalsIgnoreCase(categoryName)) {
            return "Shopping";
        }

        return categoryName;
    }
}
