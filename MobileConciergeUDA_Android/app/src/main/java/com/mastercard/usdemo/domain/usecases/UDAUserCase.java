package com.mastercard.usdemo.domain.usecases;

import android.content.Context;

import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.domain.mapper.profile.MapProfile;
import com.mastercard.usdemo.domain.model.Profile;

import io.reactivex.Completable;

public class UDAUserCase {

    private CreateProfileCase createProfileCase;
    private CheckPassCode checkPassCode;
    private String passCodeStorageCache;


    public UDAUserCase(CheckPassCode checkPassCode) {
        MapProfileApp mapLogic = new MapProfileApp();

//        createProfileCase = new CreateProfileCase(context, mapLogic);

        this.checkPassCode = checkPassCode;
    }

//    public Completable signIn(SignInCase.Params params) {
//        return checkPassCode.passCodeStorage()
//                .zipWith(signInCase.getProfile(params),
//                        (passCode, profile) -> {
//                            passCodeStorageCache = passCode;
//                            return profile;
//                        })
//                .flatMapCompletable(profile ->
//                        signInCase.handleSavePassCode(profile, passCodeStorageCache, 1)
//                );
//    }

//    public Completable signUp(Profile profile, String password) {
//        //passCode empty -> no happen this case because (get passCode from splash)
//        return checkPassCode.passCodeStorage()
//                .flatMapCompletable(passCode -> {
//                    //-- add passCode from local
////                    MapProfile mapProfile =  new MapProfile();
////                    CreateProfileCase.Params params = mapProfile.createProfile(profile, password, passCode);
////                    return createProfileCase.buildUseCaseCompletable(params);
//                    return Completable.complete();
//                });
//    }
}
