package com.mastercard.usdemo.datalayer.datasource;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mastercard.usdemo.datalayer.entity.geocoder.GeoCoderResponse;
import com.mastercard.usdemo.datalayer.restapi.GeoCoderApi;
import com.mastercard.usdemo.datalayer.retro2client.AppHttpClient;
import com.mastercard.usdemo.domain.model.LatLng;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 7/31/2017.
 */

public class AppGeoCoder {

    public String getAddress(LatLng location, Context context) {
        if (isCheckGetLocation(location)) {
            if (isGooglePlayServicesAvailable(context)) {
                Geocoder coder = new Geocoder(context);
                try {
                    List<Address> list = coder.getFromLocation(location.latitude, location.longitude, 1);
                    if (list == null || list.isEmpty())
                        return "";
                    else {
                        Address address = list.get(0);
                        if (address != null)
                            return TextUtils.isEmpty(address.getAddressLine(0)) ? "" :  address.getAddressLine(0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
                Call<GeoCoderResponse> request = geoCoderApi.getAddress(location.toString());
                try {
                    Response<GeoCoderResponse> response = request.execute();
                    GeoCoderResponse responseBody = response.body();
                    if (response.isSuccessful() && responseBody.success()) {
                        GeoCoderResponse.AddressComponent[] addressComponents = responseBody.addressComponents();
                        StringBuilder stringBuilder = new StringBuilder(addressComponents[0].name());
                        for (int i = 1; i < addressComponents.length; i++) {
                            stringBuilder.append(", ").append(addressComponents[i].name());
                        }
                        return stringBuilder.toString();
                    } else {
                        return "";
                    }
                } catch (IOException e) {
                    return "";
                }
            }
        }
        return "";
    }

    public LatLng getLocation(String strAddress, LatLng location, Context context) {
        if (isCheckGetLocation(location)) {
            if (isGooglePlayServicesAvailable(context)) {
                Geocoder coder = new Geocoder(context);
                try {
                    if (TextUtils.isEmpty(strAddress)) {
                        return LatLng.INVALID;
                    }
                    List<Address> list = coder.getFromLocationName(strAddress, 1);
                    if (list == null || list.isEmpty()) return LatLng.INVALID;
                    else {
                        Address address = list.get(0);
                        location = new LatLng(address.getLatitude(), address.getLongitude());
                    }
                    return location;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
                Call<GeoCoderResponse> request = geoCoderApi.getLatLng(strAddress);
                try {
                    Response<GeoCoderResponse> response = request.execute();
                    GeoCoderResponse responseBody = response.body();
                    if (response.isSuccessful() && responseBody.success()) {
                        return new LatLng(responseBody.location().getLat(), responseBody.location().getLng());
                    } else {
                        return LatLng.INVALID;
                    }
                } catch (IOException e) {
                    return LatLng.INVALID;
                }
            }
        }
        return LatLng.INVALID;
    }


    private boolean isCheckGetLocation(LatLng location) {
        if (location == null || !location.isValid()) return false;
        else return true;
    }


    private boolean isGooglePlayServicesAvailable(Context context) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        return resultCode == ConnectionResult.SUCCESS;
    }


    public GeoCoderResponse getFullGeoCoder(String strAddress) {
        GeoCoderApi geoCoderApi = AppHttpClient.getInstance().geoCoderApi();
        Call<GeoCoderResponse> request = geoCoderApi.getLatLng(strAddress);
        try {
            Response<GeoCoderResponse> response = request.execute();
            GeoCoderResponse responseBody = response.body();
            if (response.isSuccessful() && responseBody.success()) {
                return responseBody;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
