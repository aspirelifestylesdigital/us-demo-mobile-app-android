package com.mastercard.usdemo.presentation.venuedetail;

import com.mastercard.usdemo.domain.model.explore.DiningDetailItem;
import com.mastercard.usdemo.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface DiningDetail {

    interface View {
        void onGetDiningDetailFinished(DiningDetailItem diningDetailItem);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getDiningDetail(Integer categoryId, Integer itemId);
    }

}
