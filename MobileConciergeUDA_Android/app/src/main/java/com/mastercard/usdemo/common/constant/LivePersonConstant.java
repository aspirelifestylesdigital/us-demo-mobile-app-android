package com.mastercard.usdemo.common.constant;

import com.mastercard.usdemo.BuildConfig;

/**
 * Created by nishant.singh on 30-03-2018.
 */

public interface LivePersonConstant {
    String BrandID = "2022139";
    String AppID = BuildConfig.APPLICATION_ID;
}
