package com.mastercard.usdemo.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.widget.EditText;

import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.mastercard.usdemo.common.constant.CityData;
import com.mastercard.usdemo.common.logic.EncryptData;
import com.mastercard.usdemo.domain.mapper.profile.MapDataProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by vinh.trinh on 4/27/2017.
 */
public class Profile implements Parcelable {

    public static final int PHONE_MAX_LENGTH = 15;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String salutation;
    private boolean locationOn;

    private String question;

    public boolean anyIsEmpty() {
        return getFirstName().length() == 0 || getLastName().length() == 0 || getEmail().length() == 0 ||
                getPhone().length() == 0;
    }

    public boolean allIsEmpty() {
        return getFirstName().length() == 0 && getLastName().length() == 0 && getEmail().length() == 0 &&
                getPhone().length() == 0;
    }

    public boolean anyEmpty(EditText editTextPwd, EditText editeTextConfirmPwd) {
        return getFirstName().length() == 0 || getLastName().length() == 0 || getEmail().length() == 0 ||
                getPhone().length() == 0 || editTextPwd.getText().length() == 0 || editeTextConfirmPwd.getText().length() == 0
                || allIsEmpty();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        if (phone != null) {
            if (phone.length() > PHONE_MAX_LENGTH) {
                phone = phone.replace("+", "");
                phone = phone.substring(0, PHONE_MAX_LENGTH);
            }
        }
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDisplayCountryCodeAndPhone() {
        String valPhone = getPhone();
        if (valPhone == null || valPhone.isEmpty()) {
            return "";
        }
        String valuePhone = getPhone().replace("+", "");
        if (valuePhone.length() >= PHONE_MAX_LENGTH) {
            //don't do anything => hold original value phone
        } else {
            valuePhone = "+" + valuePhone;
        }
        return valuePhone;
    }


    public boolean isLocationOn() {
        return locationOn;
    }

    public void locationToggle(boolean on) {
        this.locationOn = on;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Profile() {
        String temp = "";
        firstName = temp;
        lastName = temp;
        email = temp;
        phone = temp;
        salutation = temp;
        question = temp;
    }


    public void addPlusSignAtTheFirstIfAny() {
        if (getPhone() != null && !getPhone().startsWith("+")) {
            setPhone("+" + getPhone());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.salutation);
        dest.writeByte(this.locationOn ? (byte) 1 : (byte) 0);
        dest.writeString(this.question);
    }

    protected Profile(Parcel in) {
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.salutation = in.readString();
        this.locationOn = in.readByte() != 0;
        this.question = in.readString();
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
