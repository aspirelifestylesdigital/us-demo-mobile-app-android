package com.mastercard.usdemo.presentation.explore;

import android.content.Context;

import com.mastercard.usdemo.domain.model.LatLng;
import com.mastercard.usdemo.domain.model.explore.ExploreRView;
import com.mastercard.usdemo.domain.model.explore.ExploreRViewItem;
import com.mastercard.usdemo.presentation.base.BasePresenter;

import java.util.List;


/**
 * Created by tung.phan on 5/8/2017.
 */

public interface Explore {
    interface View {
        void showLoading(boolean more);
        void hideLoading(boolean more);
        void updateRecommendAdapter(List<ExploreRViewItem> datum);
        List<ExploreRViewItem> historyData();
        void onUpdateFailed(String message);
        void emptyData();
        void onSearchNoData();
        void clearSearch(boolean repeat);
        void noInternetMessage();
        void cityGuideNotAvailable();
    }

    interface Presenter extends BasePresenter<View> {
        void getAccommodationData();
        void handleCitySelection();
        void handleCategorySelection(String category);
        void cityGuideCategorySelection(int index);
        void searchByTerm(String term, boolean withOffers);
        void clearSearch();
        void offHasOffers();
        void loadMore();
        ExploreRView detailItem(ExploreRView.ItemType type, int index);
        void applyDiningSortingCriteria(LatLng location, String cuisine, Context context);
    }
}
