package com.mastercard.usdemo.presentation.preferences;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.common.constant.ResultCode;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;
import com.mastercard.usdemo.presentation.base.CommonActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;

public class UserPreferencesActivity extends CommonActivity implements UserPreferences.View,
        UserPreferencesFragment.PreferencesListener {

    private DialogHelper dialogHelper;
    private UserPreferencesFragment preferencesFragment;
    private UserPreferencesPresenter presenter;
    PreferenceData original;

    private UserPreferencesPresenter presenter() {
        return new UserPreferencesPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acticity_dummy_content);
        setTitle(R.string.title_preferences);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , UserPreferencesFragment.newInstance()
                            , UserPreferencesFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PREFERENCES.getValue());

        }
        dialogHelper = new DialogHelper(this);
        presenter = presenter();
        presenter.attach(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadPreferencesOnUI(PreferenceData data) {
        original = data;
        preferencesFragment.load(data);
    }

    @Override
    public void showPreferenceSavedDialog() {
        dialogHelper.alert(null, getString(R.string.preference_updated_message));
        presenter.loadPreferences();
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg))
            return;
        if (ErrorApi.isGetTokenError(extraMsg)) dialogHelper.showGetTokenError();
//        else if(errCode == ErrCode.API_ERROR) dialogHelper.alert("ERROR!", extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void hideLoading() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showLoading() {
        dialogHelper.showProgressCancelableUnEnable();
    }

    @Override
    public void onFragmentCreated() {
        preferencesFragment = (UserPreferencesFragment) getSupportFragmentManager()
                .findFragmentByTag(UserPreferencesFragment.class.getSimpleName());
        presenter.loadPreferencesLocal();
        presenter.loadPreferences();
    }

    @Override
    public void onPreferencesSubmitted(String cuisine, String hotel, String transportation) {
        setResult(ResultCode.RESULT_OK);
        presenter.savePreferences(original.applyChanges(cuisine, hotel, transportation));
    }

    @Override
    public void onPreferencesCancel() {
        presenter.loadPreferencesLocal();
    }
}
