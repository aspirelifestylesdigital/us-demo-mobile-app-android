package com.mastercard.usdemo.domain.model.explore;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

@SuppressLint("ParcelCreator")
public class OtherExploreDetailItem extends ExploreRViewItem {

    public String benefit;
    public String termsOfUse;
    public String category;

    public OtherExploreDetailItem(int id, String title, String description, String imageURL, boolean hasStar, String summary, int dataIndex) {
        super(id, title, description, imageURL, hasStar, summary, dataIndex);
    }

    public OtherExploreDetailItem setBenefit(String benefit){
        this.benefit = benefit;
        return this;
    }
    public OtherExploreDetailItem setTermsOfUse(String termsOfUse){
        this.termsOfUse = termsOfUse;
        return this;
    }
    public OtherExploreDetailItem setCategory(String category){
        this.category = category;
        return this;
    }
    public String getDescriptionExcludeImageTag(){
        if(description.contains("<img")){
            int boldTagFoundInd = description.indexOf("<b>");
            if(boldTagFoundInd > -1) {
                return description.substring(boldTagFoundInd);
            }
        }
        return description;
    }
    public String getImageTagFromDescription(){
        if(description.contains("<img")){
            int boldTagFoundInd = description.indexOf("<b>");
            if(boldTagFoundInd > -1) {
                return description.substring(0, boldTagFoundInd);
            }
        }
        return "";
    }
    public String getDisplayTermsOfUse(){
        if(TextUtils.isEmpty(termsOfUse)){
            return App.getInstance().getApplicationContext().getString(R.string.mastercard_text);
        }
        return (termsOfUse + "<br/><br/>" + App.getInstance().getApplicationContext().getString(R.string.mastercard_text));
    }
}
