package com.mastercard.usdemo.presentation.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.presentation.base.BaseActivity;
import com.mastercard.usdemo.presentation.checkout.NewPassCodeActivity;
import com.mastercard.usdemo.presentation.checkout.PassCodeCheckout;
import com.mastercard.usdemo.presentation.checkout.PassCodeCheckoutPresenter;
import com.mastercard.usdemo.presentation.checkout.SignInActivity;
import com.mastercard.usdemo.presentation.home.HomeActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashscreenActivity extends BaseActivity implements PassCodeCheckout.View {

    @BindView(R.id.fullscreen_content)
    View contentView;
    DialogHelper dialogHelper;
    private PassCodeCheckoutPresenter presenter;

    private PassCodeCheckoutPresenter passCodeCheckoutPresenter() {
        return new PassCodeCheckoutPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        hide();
        dialogHelper = new DialogHelper(this);
        presenter = passCodeCheckoutPresenter();
        presenter.attach(this);
        if (!BuildConfig.DEBUG && isRootedDevice()) {
            dialogHelper.alert(null, getString(R.string.text_device_rooted_error), dialogInterface -> finish());
        } else {
            navigate();
        }
        // Track GA
        App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SPLASH.getValue());
        //navigate();
    }

    void navigate() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            PreferencesStorageAspire preferencesStorage = presenter.getPreferencesStorage();
            //new LoadDataWorker().load(preferencesStorage);
            boolean profileCreated = preferencesStorage.profileCreated();
            boolean hasForgotPwd = preferencesStorage.hasForgotPwd();

            if (!profileCreated || hasForgotPwd) {
                //-- non profile -> focus screen input passCode
                toSignInScreen();
            } else {
                presenter.handleSplashCheckPassCode();
            }
        }, 3000);
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (contentView != null) {
            contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private boolean isRootedDevice() {
        boolean found = false;
        String[] places = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/system/xbin/sudo",
                "/data/local/xbin/su", "/data/local/bin/su",
                "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su",
                "/magisk/.core/bin/su", "/su/bin/su"};
        for (String where : places) {
            if (new File(where).exists()) {
                found = true;
                break;
            }
        }
        return found;
    }

    //-- Fix #add PassCode
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        dismissProgressDialog();
        boolean status = dialogHelper.networkUnavailability(errCode, null);
        if (!status) {
            dialogHelper.alert(App.getInstance().getString(R.string.dialogTitleError), extraMsg);
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onCheckPassCodeSuccessfully() {
        openHomeScreen();
    }

    @Override
    public void onCheckPassCodeFailure() {
        openInputPassCodeScreen();
    }

    @Override
    public void onCheckPassCodeNone() {
        openInputPassCodeScreen();
    }

    private void openInputPassCodeScreen() {
        if (dialogHelper != null) {
            dialogHelper.actionSplash(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), getString(R.string.text_ok), getString(R.string.text_cancel),
                    (dialogInterface, i) -> toNewPassCodeScreen(),
                    (dialogInterface, i) -> toSignInScreen());
        }
    }

    private void toNewPassCodeScreen() {
        Intent intent = new Intent(this, NewPassCodeActivity.class);
        startActivity(intent);
        finish();
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    private void openHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void updatePassCodeApiCompleted() {
        //-- dummy, don't use here
    }

    @Override
    public void onErrorGetToken() {
        if (dialogHelper != null) {
            dialogHelper.showGetTokenError();
        }
    }
}