package com.mastercard.usdemo.presentation.search;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.common.constant.ResultCode;
import com.mastercard.usdemo.presentation.base.CommonActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;
import com.mastercard.usdemo.presentation.widget.ViewUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tung.phan on 5/9/2017.
 */

public class SearchActivity extends CommonActivity {

    @BindView(R.id.main_search_view)
    View mainSearchView;
    @BindView(R.id.edt_search)
    EditText edtSearchBox;
    @BindView(R.id.checkBox_offers)
    AppCompatCheckBox checkBoxOffers;
    @BindView(R.id.btn_search_submit)
    AppCompatButton buttonSubmit;
    private boolean submitted = false;
    private DialogHelper dialogHelper;

    /**
     * skip event when touch btn Cancel
     */
    private Rect mViewRectBtnSearch;
    /**
     * skip event touch editText
     */
    private Rect mViewRectGroupEdt;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setTitle("Search");
        dialogHelper = new DialogHelper(this);
        /*ViewCompat.setBackgroundTintList(checkBoxOffers,  ContextCompat.getColorStateList(this,
                R.color.button_tint_color));*/
        checkBoxOffers.setSupportButtonTintList(ContextCompat.getColorStateList(this,
                R.color.checkbox_tint_color));
        ViewUtils.drawableStart(edtSearchBox, R.drawable.ic_search_small);
        checkBoxOffers.setEnabled(false);
        checkBoxOffers.setClickable(false);
        buttonSubmit.setEnabled(false);
        buttonSubmit.setClickable(false);
        edtSearchBox.setImeActionLabel("Search", EditorInfo.IME_ACTION_SEARCH);
        edtSearchBox.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchSubmit(edtSearchBox);
                return true;
            }
            return false;
        });
        edtSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtSearchBox.getText().toString().trim().length() == 0) {
                    checkBoxOffers.setChecked(false);
                    checkBoxOffers.setEnabled(false);
                    checkBoxOffers.setClickable(false);
                    buttonSubmit.setEnabled(false);
                    buttonSubmit.setClickable(false);
                } else {
                    checkBoxOffers.setEnabled(true);
                    checkBoxOffers.setClickable(true);
                    buttonSubmit.setEnabled(true);
                    buttonSubmit.setClickable(true);
                }
            }
        });

        mainSearchView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                mainSearchView.getWindowVisibleDisplayFrame(r);
                int screenHeight = mainSearchView.getRootView().getHeight();

                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    edtSearchBox.setCursorVisible(true);
                } else {
                    // keyboard is closed
                    edtSearchBox.setCursorVisible(false);
                }
            }
        });
        toolbar.setClickable(true);


        // Suggested keyword
        String suggestedKeyword = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        if(!TextUtils.isEmpty(suggestedKeyword)){
            edtSearchBox.setText(suggestedKeyword);
            edtSearchBox.setSelection(suggestedKeyword.length());
        }
        checkBoxOffers.setChecked(getIntent().getBooleanExtra(Intent.EXTRA_REFERRER, false));

        final float scale = this.getResources().getDisplayMetrics().density;
        checkBoxOffers.setPadding(checkBoxOffers.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
                checkBoxOffers.getPaddingTop(),
                checkBoxOffers.getPaddingRight(),
                checkBoxOffers.getPaddingBottom());
        checkBoxOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
               hideSoftKeyboard(SearchActivity.this);
            }
        });

        buttonSubmit.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            mViewRectBtnSearch = new Rect();
            buttonSubmit.getGlobalVisibleRect(mViewRectBtnSearch); //response result in Rect
        });

        // space group of EditView
        edtSearchBox.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            mViewRectGroupEdt = new Rect();
            edtSearchBox.getGlobalVisibleRect(mViewRectGroupEdt);
        });
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SEARCH.getValue());
        }
    }

    @OnClick(R.id.btn_search_submit)
    void searchSubmit(View view) {
        String term = edtSearchBox.getText().toString().trim();
        boolean withOffers = checkBoxOffers.isChecked();
        if(TextUtils.isEmpty(term) && !withOffers) {
            edtSearchBox.setText("");
            dialogHelper.alert("ALERT!", getString(R.string.search_input_required));
            return;
        }
        submitted = true;
        Intent data = new Intent();
        data.putExtra(Intent.EXTRA_TEXT, term);
        data.putExtra(Intent.EXTRA_REFERRER, withOffers);
        setResult(ResultCode.RESULT_OK, data);
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if(!submitted) setResult(ResultCode.RESULT_CANCELED);
        super.onDestroy();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN
                && mViewRectBtnSearch != null
                && mViewRectGroupEdt != null) {

            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);

                int posX = (int) ev.getRawX();
                int posY = (int) ev.getRawY();

                boolean edtRect = outRect.contains(posX, posY);
                /*skip event change between 2 editText*/
                boolean groupEdtViewRect = mViewRectGroupEdt.contains(posX, posY);
                boolean btnCancelRectTouchIn = mViewRectBtnSearch.contains(posX, posY);
                boolean btnSearchEnable = buttonSubmit.isEnabled();

                //touch btn cancel and unEnable
                if (btnCancelRectTouchIn) {
                    if (!btnSearchEnable) {
                        v.clearFocus();
                        ViewUtils.hideSoftKey(v);
                    }//else handle when onClick
                } else if (!edtRect && !groupEdtViewRect) {
                    v.clearFocus();
                    ViewUtils.hideSoftKey(v);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }
}
