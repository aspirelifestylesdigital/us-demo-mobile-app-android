package com.mastercard.usdemo.presentation.profile;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.domain.mapper.profile.MapProfile;
import com.mastercard.usdemo.domain.model.Profile;
import com.mastercard.usdemo.domain.usecases.UDAUserCase;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public class CreateProfilePresenter implements CreateProfile.Presenter {

    private CompositeDisposable disposables;
    private CreateProfile.View view;
    private UDAUserCase udaUserCase;

    CreateProfilePresenter(Context context) {
        disposables = new CompositeDisposable();
//        this.udaUserCase = new UDAUserCase(context);
    }

    @Override
    public void attach(CreateProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void createProfile(Profile profile, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
//        disposables.add(
//                udaUserCase.signUp(profile, password)
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribeWith(new SaveProfileObserver())
//        );
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.dismissProgressDialog();
            view.showProfileCreatedDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;
            view.dismissProgressDialog();
            if (ErrCode.PASS_CODE_ERROR.name().equals(e.getMessage())) {
                view.onErrorPassCode();
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
            dispose();
        }
    }

}
