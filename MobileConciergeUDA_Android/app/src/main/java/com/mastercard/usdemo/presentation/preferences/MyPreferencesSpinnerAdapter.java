package com.mastercard.usdemo.presentation.preferences;


import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import com.mastercard.usdemo.R;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.List;

public class MyPreferencesSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private List<String> asr;
    private boolean isFirstTime = true;
    LayoutInflater inflater;
    private Context context;

    public MyPreferencesSpinnerAdapter(Context ctx, List<String> asr) {
        this.asr = asr;
        this.context = ctx;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return asr.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    public Object getItem(int i) {
        return asr.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomDropView(position, convertView, parent);
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        return getCustomView(i, view, viewgroup);
    }

    public void setFirstTime(boolean firstTime) {
        this.isFirstTime = firstTime;
    }

    public boolean getFirstTime() {
        return isFirstTime;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.preference_spinner_item, parent, false);
        LetterSpacingTextView txt = (LetterSpacingTextView) row.findViewById(R.id.title);
        View view = row.findViewById(R.id.view);
        view.setVisibility(View.GONE);
        txt.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
        txt.setMaxLines(1);
        txt.setEllipsize(TextUtils.TruncateAt.END);
        if (position == 0) {
            txt.setText(asr.get(0).toString());
            txt.setTextColor(Color.parseColor("#99A1A8"));
            isFirstTime = false;
            notifyDataSetChanged();
        } else {
            txt.setText(asr.get(position).toString());
            txt.setTextColor(Color.parseColor("#011627"));
        }

        return row;
    }

    public View getCustomDropView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.preference_spinner_item_dropdown, parent, false);
        LetterSpacingTextView txt = (LetterSpacingTextView) row.findViewById(R.id.title);
        View view = row.findViewById(R.id.view);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setTextColor(Color.parseColor("#011627"));

        if (position == 0) {
            txt.setText("None");
        } else {
            txt.setText(asr.get(position).toString());
            if (position == asr.size() - 1) {
                view.setVisibility(View.GONE);
            }
        }

        return row;
    }
}