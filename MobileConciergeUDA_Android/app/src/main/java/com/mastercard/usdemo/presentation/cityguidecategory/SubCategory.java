package com.mastercard.usdemo.presentation.cityguidecategory;

import com.mastercard.usdemo.domain.model.SubCategoryItem;
import com.mastercard.usdemo.presentation.base.BasePresenter;

import java.util.List;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface SubCategory {

    interface View {
        void showLoading();

        void hideLoading();

        void updateSubCategoryRViewAdapter(List<SubCategoryItem> subCategoryItems);
    }

    interface Presenter extends BasePresenter<View> {

        void getSubCategories(int categoryId);
    }
}
