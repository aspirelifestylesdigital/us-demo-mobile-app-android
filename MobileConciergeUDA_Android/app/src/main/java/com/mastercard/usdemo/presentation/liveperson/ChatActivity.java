package com.mastercard.usdemo.presentation.liveperson;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.LoadProfile;
import com.liveperson.infra.ConversationViewParams;
import com.liveperson.infra.ICallback;
import com.liveperson.infra.LPAuthenticationParams;
import com.liveperson.infra.messaging_ui.fragment.ConversationFragment;
import com.liveperson.messaging.sdk.api.LivePerson;
import com.liveperson.messaging.sdk.api.model.ConsumerProfile;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.BuildConfig;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;
import com.mastercard.usdemo.presentation.base.CommonActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;
import com.mastercard.usdemo.push.fcm.FCMUtils;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by nishant.singh on 20-03-2018.
 * Used as a LivePerson Fragment container.
 */

public class ChatActivity extends CommonActivity implements LivepersonIntentHandler.ChatActivityCallback {
    private static final String TAG = ChatActivity.class.getSimpleName();
    private static final String LIVEPERSON_FRAGMENT = "liveperson_fragment";
    private ConversationFragment mConversationFragment;
    private LoadProfile mloLoadProfile;
    private DialogHelper mDialogHelper;
    private Menu mMenu;
    private static CompositeDisposable compositeDisposable;
    // Intent Handler
    private LivepersonIntentHandler mIntentsHandler;


    private LoadProfile loadProfileUseCase() {
        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(this);
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage,mapLogic);
        return new LoadProfile(profileRepository);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        setTitle(R.string.chat);
        mDialogHelper = new DialogHelper(this);
        setToolbarColor(R.color.colorPrimaryDark);
        App.getInstance().initLivePerson();
        initFragment();
        compositeDisposable = new CompositeDisposable();
        mIntentsHandler = new LivepersonIntentHandler(this);
        setUserProfile();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    private void setUserProfile() {
        mloLoadProfile = loadProfileUseCase();
        Single<ProfileAspire> profileSingle = mloLoadProfile.loadStorage();
        Disposable disposable = profileSingle.subscribe(profile -> {
            FCMUtils.handleGCMRegistration(this);
            ConsumerProfile consumerProfile = new ConsumerProfile.Builder()
                    .setFirstName(profile.getFirstName())
                    .setLastName(profile.getLastName())
                    .setPhoneNumber(profile.getPhone() + "|" + profile.getEmail())
                    .build();
            LivePerson.setUserProfile(consumerProfile);
            Log.d(TAG, profile.toString() + "consumer profile set");
        }, throwable -> {
            Log.e(TAG, "Error in getting profile");
        });
        compositeDisposable.add(disposable);


    }

    private void initFragment() {
        mConversationFragment = (ConversationFragment) getSupportFragmentManager().findFragmentByTag(LIVEPERSON_FRAGMENT);
        Log.d(TAG, "initFragment. mConversationFragment = " + mConversationFragment);
        if (mConversationFragment == null) {
//            String authCode = SampleAppStorage.getInstance(ChatActivity.this).getAuthCode();
//            String publicKey = SampleAppStorage.getInstance(ChatActivity.this).getPublicKey();
            String authCode = "";
            String publicKey = "";

            Log.d(TAG, "initFragment. authCode = " + authCode);
            Log.d(TAG, "initFragment. publicKey = " + publicKey);
            LPAuthenticationParams authParams = new LPAuthenticationParams();
            authParams.setAuthKey(authCode);
            authParams.addCertificatePinningKey(publicKey);
            mConversationFragment = (ConversationFragment) LivePerson.getConversationFragment(authParams, new ConversationViewParams(false));

            if (isValidState()) {

                // Pending intent for image foreground service
                Intent notificationIntent = new Intent(this, ChatActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
                LivePerson.setImageServicePendingIntent(pendingIntent);

                // Notification builder for image upload foreground service
                Notification.Builder uploadBuilder = new Notification.Builder(this.getApplicationContext());
                Notification.Builder downloadBuilder = new Notification.Builder(this.getApplicationContext());
                uploadBuilder.setContentTitle("Uploading image")
                        .setSmallIcon(android.R.drawable.arrow_up_float)
                        .setContentIntent(pendingIntent)
                        .setProgress(0, 0, true);

                downloadBuilder.setContentTitle("Downloading image")
                        .setSmallIcon(android.R.drawable.arrow_down_float)
                        .setContentIntent(pendingIntent)
                        .setProgress(0, 0, true);

                LivePerson.setImageServiceUploadNotificationBuilder(uploadBuilder);
                LivePerson.setImageServiceDownloadNotificationBuilder(downloadBuilder);


                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.custom_fragment_container, mConversationFragment, LIVEPERSON_FRAGMENT).commitAllowingStateLoss();
            }
        } else {
            attachFragment();
        }
    }


    private boolean isValidState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return !isFinishing() && !isDestroyed();
        } else {
            return !isFinishing();
        }
    }

    private void attachFragment() {
        if (mConversationFragment.isDetached()) {
            Log.d(TAG, "initFragment. attaching fragment");
            if (isValidState()) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.attach(mConversationFragment).commitAllowingStateLoss();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mConversationFragment != null) {
            attachFragment();
        }
    }


    @Override
    public void onBackPressed() {
        if (mConversationFragment == null || !mConversationFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.clear_history:
                // check if the history is resolved,if not skip the clear command and notify the user.
                mDialogHelper.action(getString(R.string.clear_history),
                        getString(R.string.alert_clear_history),
                        getString(R.string.text_clear), getString(R.string.text_cancel),
                        (dialog, which) -> {
                            LivePerson.checkActiveConversation(new ICallback<Boolean, Exception>() {
                                @Override
                                public void onSuccess(Boolean aBoolean) {
                                    if (!aBoolean) {
                                        //clear history only from device
                                        LivePerson.clearHistory();
                                    } else {
                                        mDialogHelper.alert(getString(R.string.clear_history), getString(R.string.resolve_conversation_first));
                                    }

                                }

                                @Override
                                public void onError(Exception e) {
                                    Log.e(TAG, e.getMessage());
                                }
                            });

                        });
                break;
            case R.id.mark_as_resolved:
                mDialogHelper.action(getString(R.string.resolve_conversation),
                        getString(R.string.alert_resolve_chat),
                        getString(R.string.text_yes), getString(R.string.text_cancel),
                        (dialog, which) -> {
                            LivePerson.resolveConversation();
                        });
                break;
            case R.id.mark_as_urgent:
                if (item.getTitle().toString().equalsIgnoreCase(getString(R.string.mark_as_urgent))) {
                    showMarkAsUrgentDialogue();
                } else {
                    showMarkAsNormalDialogue();
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (mIntentsHandler.getIsConversationUrgent()) {
            menu.getItem(0).setTitle(getResources().getString(R.string.dismiss_urgency));
        } else {
            menu.getItem(0).setTitle(getResources().getString(R.string.mark_as_urgent));
        }

        if (mIntentsHandler.getIsConversationActive()) {
            menu.setGroupEnabled(R.id.grp_urgent, true);
        } else {
            menu.setGroupEnabled(R.id.grp_urgent, false);
        }


        if (mIntentsHandler.getIsCsatLaunched()) {
            menu.setGroupEnabled(R.id.grp_clear, false);
        }
        return true;
    }

    private void showMarkAsUrgentDialogue() {
        mDialogHelper.action(getString(R.string.alert_mark_as_urgent),
                getString(R.string.alert_urgent_subject),
                getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialog, which) -> {
                    LivePerson.markConversationAsUrgent();
                    mIntentsHandler.setIsConversationUrgent(true);
                });
    }

    private void showMarkAsNormalDialogue() {
        mDialogHelper.action(getString(R.string.alert_dismiss_urgent),
                getString(R.string.alert_dismiss_urgent_subject),
                getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialog, which) -> {
                    LivePerson.markConversationAsNormal();
                    mIntentsHandler.setIsConversationUrgent(false);
                });
    }


    @Override
    public void finishChatScreen() {
        finish();
    }

    @Override
    public void setAgentName(String agentName) {
        setTitle(agentName);
    }

    @Override
    public void closeOptionMenu() {
        if (mMenu != null)
            mMenu.close();
    }
}
