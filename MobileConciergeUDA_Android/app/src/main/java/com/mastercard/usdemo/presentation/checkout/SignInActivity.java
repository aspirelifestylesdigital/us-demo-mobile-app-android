package com.mastercard.usdemo.presentation.checkout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.common.constant.IntentConstant;
import com.mastercard.usdemo.common.constant.RequestCode;
import com.mastercard.usdemo.common.logic.Validator;
import com.mastercard.usdemo.presentation.base.BaseActivity;
import com.mastercard.usdemo.presentation.changepass.ChangePasswordActivity;
import com.mastercard.usdemo.presentation.home.HomeActivity;
import com.mastercard.usdemo.presentation.profile.forgotPwdV2.ForgotPasswordV2Activity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;
import com.mastercard.usdemo.presentation.widget.PasswordInputFilter;
import com.support.mylibrary.widget.ButtonTextSpacing;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements SignIn.View {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_password)
    ErrorIndicatorEditText edtPassword;
    @BindView(R.id.btn_sign_in)
    ButtonTextSpacing btnSignIn;
    @BindView(R.id.img_background)
    ImageView imgBackground;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    DialogHelper dialogHelper;
    SignInPresenter presenter;
    Validator validator;
    String errMessage;

    private SignInPresenter signInPresenter() {
        return new SignInPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        final Handler handler = new Handler();
        dialogHelper = new DialogHelper(this);
        presenter = signInPresenter();
        presenter.attach(this);
        validator = new Validator();
        resizeBackgroundImage();

        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                hideKeyboard(view1);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {
                    }
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
        });

        edtPassword.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                hideKeyboard(view1);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {
                    }
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isEnabled = editable.toString().length() > 0
                        && edtPassword.getText().toString().length() > 0;
                btnSignIn.setEnabled(isEnabled);
                btnSignIn.setClickable(isEnabled);
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isEnabled = editable.toString().length() > 0
                        && edtEmail.getText().toString().length() > 0;
                btnSignIn.setEnabled(isEnabled);
                btnSignIn.setClickable(isEnabled);
            }
        });
        edtPassword.setCustomSelectionActionModeCallback(new android.view.ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
            }
        });
        edtPassword.setFilters(new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))});
        btnSignIn.post(new Runnable() {
            @Override
            public void run() {
                btnSignIn.setClickable(false);
            }
        });
        /*edtEmail.setText("viet17@gmail.com");
        edtPassword.setText("123456");*/
        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_IN.getValue());
        }
        ClickGuard.guard(findViewById(R.id.tv_sign_up),
                findViewById(R.id.tv_forgot_pw));
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_sign_in)
    public void signInClick(View view) {
        hideKeyboard(view);
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();
        if (validateFields(email, password)) {
            edtEmail.setError(null);
            edtPassword.setError(null);
            presenter.doSignIn(email, password);
        } else {
            dialogHelper.profileDialog(errMessage, dialog -> {
                if (view != null) {
                    view.postDelayed(() -> invokeSoftKey(view), 100);

                }
            });
        }
    }

    @OnClick(R.id.tv_sign_up)
    public void toSignUpView() {
        Intent intent = new Intent(this, NewPassCodeActivity.class);
        intent.putExtra(IntentConstant.SIGN_UP, true);
        startActivityForResult(intent, RequestCode.SIGN_IN_SIGN_UP);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearForm();
            }
        }, 500);
    }

    @OnClick(R.id.tv_forgot_pw)
    public void forgotPasswordClick() {
        Intent intent = new Intent(getApplicationContext(), ForgotPasswordV2Activity.class);
        startActivityForResult(intent, RequestCode.SIGN_IN_FORGOT);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearForm();
            }
        }, 500);
    }

    // presenter
    @Override
    public void proceedToHome() {
        boolean hasForgotPwd = new PreferencesStorageAspire(getApplicationContext()).hasForgotPwd();
        Intent intent;
        if (hasForgotPwd) {
            intent = new Intent(this, ChangePasswordActivity.class);
            intent.putExtra(Intent.EXTRA_REFERRER, "a");
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        startActivity(intent);
        finish();

        // Track GA with "Sign in" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_IN.getValue());
    }

    @Override
    public void onCheckPassCodeFailure(ProfileAspire profileAspireCache) {
//        dialogHelper.alert(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), v ->{
//            Intent intent = new Intent(this, NewPassCodeActivity.class);
//            startActivity(intent);
//            finish();
//        });


        dialogHelper.action(App.getInstance().getString(R.string.dialogTitleError), getString(R.string.dialogErrorNonePassCode), getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialogInterface, i) -> toNewPassCodeScreen(profileAspireCache));
    }

    private void toNewPassCodeScreen(ProfileAspire profileAspireCache) {
        Intent intent = new Intent(this, NewPassCodeActivity.class);
        try {
            String valueAspire = profileAspireCache.toJSON().toString();
            intent.putExtra(IntentConstant.SIGN_IN_PROFILE, valueAspire);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(intent);
        finish();
    }

    private void toSignInScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }

    // presenter
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        edtEmail.setError("");
        edtPassword.setError("");
        if (errCode == ErrCode.API_ERROR) {
            dialogHelper.alert(App.getInstance().getString(R.string.errorTitle),extraMsg);
        } else {
            dialogHelper.showGeneralError();
        }
        if (getCurrentFocus() != null) {
            getCurrentFocus().clearFocus();
        }
    }

    // presenter
    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    // presenter
    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    private boolean validateFields(String email, String password) {
        boolean valid = true;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.errorForceSignOut));

        if (TextUtils.isEmpty(email.trim()) || TextUtils.isEmpty(password.trim())) {
            if (TextUtils.isEmpty(email)) edtEmail.setError("");
            if (TextUtils.isEmpty(password)) edtPassword.setError("");
            else edtPassword.setError(null);
//            stringBuilder.append(getString(R.string.input_err_required));
            valid = false;
        }
        if (!TextUtils.isEmpty(email) && !validator.email(email)) {
            edtEmail.setError("");
            //if (stringBuilder.length() > 0) stringBuilder.append("\n");
//            stringBuilder.append(getString(R.string.errorForceSignOut));
            valid = false;
        }

        if (TextUtils.isEmpty(password) || password.length() < 5) {
            edtPassword.setError("");
//            if (stringBuilder.length() > 0) stringBuilder.append("\n");
//            stringBuilder.append(getString(R.string.input_err_old_pwd_failed));
            valid = false;
        }

        if (!valid) {
            errMessage = stringBuilder.toString();
        }
        return valid;
    }

    private void resizeBackgroundImage() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        imgBackground.getLayoutParams().height = height;
        imgBackground.getLayoutParams().width = width;
        imgBackground.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    void invokeSoftKey(View view) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                getWindow().getCurrentFocus().getWindowToken(),
                InputMethodManager.SHOW_IMPLICIT, 0);
    }

    private void clearForm() {
        if (edtEmail == null || edtPassword == null)
            return;
        edtEmail.setText("");
        edtPassword.setText("");

        edtEmail.hideErrorColorLine();
        edtPassword.hideErrorColorLine();

        if (getCurrentFocus() != null) {
            getCurrentFocus().clearFocus();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case RequestCode.SIGN_IN_FORGOT :
                    edtEmail.setText(data.getStringExtra(IntentConstant.EMAIL_VALUES_FORGOT));
                    break;
            }
        }
    }
}
