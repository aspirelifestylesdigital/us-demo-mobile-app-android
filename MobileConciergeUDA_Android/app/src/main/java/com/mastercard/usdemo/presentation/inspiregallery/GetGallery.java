package com.mastercard.usdemo.presentation.inspiregallery;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.usdemo.domain.model.GalleryViewPagerItem;
import com.mastercard.usdemo.presentation.base.BasePresenter;

import java.util.List;

/**
 * Created by Thu Nguyen on 6/13/2017.
 */

public interface GetGallery {

    interface View {
        void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList);
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {
        void getGalleryList();
    }

}
