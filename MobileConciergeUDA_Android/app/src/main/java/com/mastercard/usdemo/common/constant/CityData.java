package com.mastercard.usdemo.common.constant;

import android.text.TextUtils;
import android.util.SparseArray;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public final class CityData {
    /* warring change array cities */
    private static LinkedHashMap<String, City> cities;
    static final SparseArray<CityGuide> cityGuide;
    private static String SELECTED_CITY = "";
    public static final String DEFAULT_CITY = "All";
    public static final List<String> regions = Arrays.asList("Africa/Middle East", "Asia Pacific", "Caribbean",
            "Europe", "Latin America", "Canada");

    public static final String VALUE_GEOGRAPHIC_REGION_ALL = "all";

    //-- Fix #UDA-203
    // Update Data add ,1086,1370,1371,1372,1373,1374,1375;
    /** case: id in array, app skip process that object for get Category or SearchResult  */
    public static final List<Integer> TitleContentIDs = Arrays.asList(12,20,34,269,270,276,279,280,281,282,284,
            285,288,290,291,292,339,341,342,345,347,348,349,364,368,369,370,371,372,406,
            409,411,413,415,417,431,439,446,448,456,457,459,462,463,464,466,467,475,476,477,517,
            519,520,522,524,527,528,557,559,561,625,652,653,655,656,657,659,661,662,663,692,694,
            699,700,701,703,704,706,775,794,795,796,813,818,840,841,842,843,844,845,848,849,850,
            852,880,881,884,886,887,890,893,911,912,916,929,936,938,940,941,945,946,951,978,979,
            980,981,982,984,986,987,1091,1092,1093,1094,1095,1096,1097,1098,1100,1101,1102,1103,
            1104,1105,1106,1107,1108,1109,1110,1111,1112,1144,1145,1146,1147,1148,1149,1151,1152,
            1154,1155,1156,1158,1162,1164,1165,1166,1167,1171,1174,1175,1177,1178,1179,1181,1182,
            1183,1184,1185,1186,1187,1188,1189,1191,1192,1194,1214,1249,1309,1324,1334,25,1067,
            1070,1071,1077,1081,1115,1116,1117,1325,1326,1327,1328,1329,1330,1331,1332,1333,35,
            1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1265,1266,1267,1269,1270,1271,1274,
            1278,1279,1280,1281,1282,1284,1285,1286,1287,1302,1303,1304,1305,1306,1307,1308,1310,
            1312,1313,1314,1315,1316,1317,1318,1319,1320,1321,1322,1323,1086,1370,1371,1372,1373,1374,1375, 1384);

    static {
        cityGuide = new SparseArray<>();

        cities = new LinkedHashMap<>();

        final String atlanta = "Atlanta";
        cities.put(atlanta, new City(atlanta,"North America.Georgia",true,6514,0));
        final String boston = "Boston";
        cities.put(boston, new City(boston,"North America.Vermont.Maine.Rhode Island.New Hampshire.Massachusetts",true,6422,6311,6357,6358,6359,6360,6362));
        final String cancun = "Cancun";
        cities.put(cancun, new City(cancun,"North America.Mexico",false,6586,0));
        final String chicago = "Chicago";
        cities.put(chicago ,new City(chicago,"North America.Illinois",true,6317,6308,6339,6340,6341,6342,6344));
        final String cleveland = "Cleveland";
        cities.put(cleveland,new City(cleveland,"",true,6318,0));
        final String dallas = "Dallas";
        cities.put(dallas,new City(dallas,"North America.Texas",true,6423,6445,6452,6453,6454,6455,6457));
        final String hawaii = "Hawaii";
        cities.put(hawaii,new City(hawaii,"North America.Hawaii",true,6473,0));
        final String lasVegas = "Las Vegas";
        cities.put(lasVegas,new City(lasVegas,"North America.Arizona.Nevada",true,6319,6310,6351,6352,6353,6354,6356));
        final String london = "London";
        cities.put(london, new City(london,"Europe.United Kingdom",false,6443,6314,6375,6376,6377,6378,6380));
        final String losAngles = "Los Angeles";
        cities.put(losAngles,new City(losAngles,"North America.California",true,6320,6312,6363,6364,6365,6366,6368));
        final String miami = "Miami";
        cities.put(miami,new City(miami,"North America.Florida",true,6321,6307,6333,6334,6335,6336,6338));
        final String milwaukee = "Milwaukee";
        cities.put(milwaukee,new City(milwaukee,"North America.Wisconsin",true,6322,0));
        final String montreal = "Montreal";
        cities.put(montreal,new City(montreal,"North America.Quebec",true,6424,6429,6436,6438,6439,6440,6437));
        final String napa = "Napa/Sonoma";
        cities.put(napa,new City(napa,"North America.California",true,6483,0));
        final String newOrleans = "New Orleans";
        cities.put(newOrleans,new City(newOrleans,"North America.Lousiana",true,6515,0));
        final String newYork = "New York";
        cities.put(newYork,new City(newYork,"North America.Connecticut.New York",true,6323,6306,6326,6327,6328,6329,6331));
        final String orlando = "Orlando";
        cities.put(orlando,new City(orlando,"North America.Florida",true,6477,6389,6395,6398,6405,6408,6418));
        final String paris = "Paris";
        cities.put(paris,new City(paris,"Europe.France",false,6476,6315,6381,6382,6383,6384,6386));
        final String philadelphia = "Philadelphia";
        cities.put(philadelphia,new City(philadelphia,"North America.Delaware",true,6425,0));
        final String rome = "Rome";
        cities.put(rome,new City(rome,"Europe.Italy",false,6475,6458,6460,6461,6462,6471,6472));
        final String sanFrancisco = "San Francisco";
        cities.put(sanFrancisco,new City(sanFrancisco,"North America.California",true,6324,6313,6369,6370,6371,6372,6374));
        final String seattle = "Seattle";
        cities.put(seattle,new City(seattle,"North America.Washington",true,6426,6444,6446,6447,6448,6449,6451));
        final String toronto = "Toronto";
        cities.put(toronto,new City(toronto,"North America.Ontario",true,6427,6428,6430,6432,6433,6434,6431));
        final String vancouver = "Vancouver";
        cities.put(vancouver,new City(vancouver,"North America.British Columbia",true,6442,6459,6464,6468,6470,6465,6469));
        final String washington = "Washington, D.C.";
        cities.put(washington,new City(washington,"North America.Virgina.District of Columbia",true,6325,6309,6345,6346,6347,6348,6350));
        final String africa = "Africa/Middle East";
        cities.put(africa,new City(africa,"Africa/Middle East",false,0,0));
        final String asiaPacific = "Asia Pacific";
        cities.put(asiaPacific,new City(asiaPacific,"Asia Pacific",false,6523,0));
        final String canada = "Canada";
        cities.put(canada,new City(canada,"North America.Quebec.Ontario.British Columbia.Alberta.Canada",false,0,0));
        final String caribbean = "Caribbean";
        cities.put(caribbean,new City(caribbean,"Caribbean",false,0,0));
        final String europe = "Europe";
        cities.put(europe,new City(europe,"Europe",false,0,0));
        final String latinAmerica = "Latin America";
        cities.put(latinAmerica,new City(latinAmerica,"Latin America",false,0,0));
    }

    public static void reset() {
        SELECTED_CITY = "";
    }

    public static boolean setSelectedCity(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            SELECTED_CITY = cityName;
            return true;
        }//-- do nothing save select city
        return false;
    }

    /** check city name exist in array city */
    public static boolean isCityInApp(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            return true;
        }
        return false;
    }


    public static int selectedCity() {
        int pos = 0;
        for (Map.Entry<String, City> entry : cities.entrySet()){
            if(SELECTED_CITY.equalsIgnoreCase(entry.getKey())){
                break;
            }
            pos++;
        }
        return pos;
    }

    public static boolean citySelected() {
        return (!TextUtils.isEmpty(SELECTED_CITY) && cities.get(SELECTED_CITY) != null);
    }

    public static int diningCode() {
        if(!citySelected()) return 0;
        return cities.get(SELECTED_CITY).diningCode;
    }

    public static int guideCode() {
        if(!citySelected()) return 0;
        return cities.get(SELECTED_CITY).cityGuideCode;
    }

    public static String cityName() {
        if(!citySelected()) return DEFAULT_CITY;
        return cities.get(SELECTED_CITY).name;
    }

    public static String cityNameDefaultEmpty() {
        if(!citySelected()) return "";
        return cities.get(SELECTED_CITY).name;
    }

    public static boolean isUSCity() {
        return citySelected() && cities.get(SELECTED_CITY).usCity;
    }

    public static String geographicRegion() {
        return cities.get(SELECTED_CITY).geographic;
    }

    public static CityGuide cityGuide(int code) {
        return cityGuide.get(code);
        /*if(result == null) {
            throw new IllegalStateException("passed city code not found");
        }*/
    }

    /**
     * @param cityGuideCategoryIndex either position of spa | bar | shopping...
     * @return int value of
     */
    public static int specificCityGuideCode(int cityGuideCategoryIndex) {
        if(!citySelected())
            return -1;
        int cityCode = cities.get(SELECTED_CITY).cityGuideCode;
        if(cityCode == 0) return -1;
        CityGuide cityGuide = cityGuide(cityCode);
        if(cityGuide == null || cityGuide.size() <= cityGuideCategoryIndex) return -1;
        return cityGuide(cityCode).at(cityGuideCategoryIndex);
    }

    public static boolean isDiningItem(int subCategoryID) {
        for (Map.Entry<String, City> entry : cities.entrySet()){
            City city = entry.getValue();
            if(city.diningCode == subCategoryID) return true;
        }
        return false;
    }

    public static boolean isCityGuideItem(int catID) {
        int cityGuideCode = guideCode();
        return cityGuideCode > 0 && cityGuide(cityGuideCode).has(catID);
    }
}
