package com.mastercard.usdemo.presentation.checkout;


import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.AuthAspireRepository;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.domain.usecases.MapProfileApp;
import com.mastercard.usdemo.domain.usecases.UDAUserCase;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class SignInPresenter implements SignIn.Presenter {

    private CompositeDisposable compositeDisposable;
    private SignIn.View view;

    private CheckPassCode checkPassCode;
    private SignInCase signInCase;

    private PreferencesStorageAspire preferencesStorage;

    //- variable
    private ProfileAspire profileAspireCache;

    SignInPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();

        MapProfileApp mapLogic = new MapProfileApp();
        this.preferencesStorage = new PreferencesStorageAspire(c);
        AuthAspireRepository authRepository = new AuthDataAspireRepository();
        ProfileAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorage, mapLogic);
        LoadProfile mLoadProfile = new LoadProfile(profileRepository);
        SaveProfile saveProfile = new SaveProfile(profileRepository);
        GetToken getToken = new GetToken(preferencesStorage, mapLogic);
        GetAccessToken mGetAccessToken = new GetAccessToken(mLoadProfile, getToken);
        UserProfileOKTADataRepository userProfileOKTADataRepository = new UserProfileOKTADataRepository(new RemoteUserProfileOKTADataStore());
        this.checkPassCode = new CheckPassCode(mapLogic,
                preferencesStorage,
                authRepository,
                mGetAccessToken,
                mLoadProfile,
                saveProfile,
                getToken
        );
        this.signInCase = new SignInCase(mapLogic,
                profileRepository,
                userProfileOKTADataRepository,
                getToken);
    }

    @Override
    public void attach(SignIn.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doSignIn(String email, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();

        //<editor-fold desc="flow check passCode before sign in">
        //        Single.create(e -> {
//            String binCode = checkPassCode.getPassCodeStorage();
//            if (TextUtils.isEmpty(binCode)) {
//                e.onError(new Exception(ErrCode.PASS_CODE_ERROR.name()));
//                return;
//            }
//
//            e.onSuccess(binCode);
//
//        }).flatMapCompletable(binCode -> {
//            SignInCase.Params params = new SignInCase.Params(email, password);
//            return signInCase.getProfile(params)
//                    .flatMapCompletable(profileAspire ->
//                            signInCase.handleSavePassCode(profileAspire, (String) binCode));
//
//        })
        //</editor-fold>
        //-- flow check passCode after sign in
        SignInCase.Params params = new SignInCase.Params(email, password);
        Completable.create(e -> {
            //clean reset preference
            preferencesStorage.clear();
            e.onComplete();
        }).andThen(signInCase.loadProfileOKTACheckStatus(params.email).andThen(handleSignInCase(params)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        view.dismissProgressDialog();
                        view.proceedToHome();
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view == null) return;

                        view.dismissProgressDialog();
                        if(ErrCode.PROFILE_OKTA_LOCKED_OUT.name().equals(e.getMessage())){
                            view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.valid_login_okta_profile_error));
                        } else if (ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name().equals(e.getMessage())
                                || ErrorApi.isGetTokenError(e.getMessage())) {
                            view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.valid_login_error));
                        } else if (ErrCode.PASS_CODE_ERROR.name().equals(e.getMessage())
                                && profileAspireCache != null) {
                            view.onCheckPassCodeFailure(profileAspireCache);
                        } /*else if (ErrorApi.isGetTokenError(e.getMessage())) {
                            view.showErrorDialog(ErrCode.API_ERROR,
                                    App.getInstance().getString(R.string.errorSignIn));
                        } */else {
                            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                        }
                        dispose();
                    }
                });
    }

    private Completable handleSignInCase(SignInCase.Params params) {
        return signInCase.loadProfileNoSaveStorage(params)
                .flatMapCompletable(profileAspire -> {
                    profileAspireCache = profileAspire;
                    return checkPassCode.handleCheckPassCode(profileAspire).flatMapCompletable(resultPassCode -> {
                        if (CheckPassCode.PassCodeStatus.VALID == resultPassCode.getPassCodeStatus()) {
                            //-- save binCode local
                            String binCode = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);

                            preferencesStorage.editor().binCode(binCode).build().save();

                            return signInCase.saveProfileStorage(profileAspire).toCompletable();
                        } else {
                            return Completable.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
                        }
                    });
                }
        );
    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }

}
