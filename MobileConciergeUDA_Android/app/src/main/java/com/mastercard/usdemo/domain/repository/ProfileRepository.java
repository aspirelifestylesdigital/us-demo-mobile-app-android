package com.mastercard.usdemo.domain.repository;

import com.mastercard.usdemo.domain.model.Profile;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 5/12/2017.
 */

public interface ProfileRepository {

    Single<Profile> signInProfile(String accessToken, String password, String username);

    Single<Profile> loadProfile(String accessToken);

    Completable saveProfile(String accessToken, Profile profile);

    Completable createProfile(String serviceToken, Profile profile, String passCode);

}
