package com.mastercard.usdemo.presentation.info;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.usdemo.datalayer.entity.GetClientCopyResult;
import com.mastercard.usdemo.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface MasterCardUtility {

    interface View {
        void onGetMasterCardCopy(GetClientCopyResult result);
        void loadEmptyContent();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {
        void getMasterCardCopy(String type);
    }

}
