package com.mastercard.usdemo.presentation.selectcity;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mastercard.usdemo.App;
import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.constant.AppConstant;
import com.mastercard.usdemo.common.constant.CityData;
import com.mastercard.usdemo.common.constant.IntentConstant;
import com.mastercard.usdemo.common.constant.ResultCode;
import com.mastercard.usdemo.domain.model.CityRViewItem;
import com.mastercard.usdemo.presentation.base.CommonActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by tung.phan on 5/9/2017.
 */

public class SelectCityActivity extends CommonActivity implements SelectCity.View,
        CityRViewAdapter.CityRViewAdapterListener {

    @BindView(R.id.selection_recycle_view)
    RecyclerView selectCityRView;
    private CityRViewAdapter cityRViewAdapter;

    private SelectCityPresenter presenter;

    private boolean mBackAskScreen = false;

    private SelectCityPresenter buildPresenter() {
        return new SelectCityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_recyclerview_activity_layout);
        setTitle(R.string.title_choose_city);
        setToolbarColor(R.color.colorPrimaryDark);
        fakeCityData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        selectCityRView.setLayoutManager(layoutManager);
        selectCityRView.setAdapter(cityRViewAdapter);
        selectCityRView.addItemDecoration(new DividerItemDecoration(this));
        layoutManager.scrollToPosition(CityData.selectedCity());

        presenter = buildPresenter();
        presenter.attach(this);

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CITY_LIST.getValue());
        }

        if (getIntent() != null) {
            mBackAskScreen = getIntent().getBooleanExtra(IntentConstant.ASK_SCREEN_TO_SELECT_CITY, false);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void fakeCityData() {
        Resources res = getResources();
        TypedArray imagesResources = res.obtainTypedArray(R.array.city_images);
        String[] cities = res.getStringArray(R.array.city_list);
        String[] citiesDescriptions = res.getStringArray(R.array.city_locate_list);
        final List<CityRViewItem> cityRViewItems = new ArrayList<>();
        for (int i = 0; i < cities.length; i++) {
            cityRViewItems.add(new CityRViewItem(
                    imagesResources.getResourceId(i, R.drawable.img_placeholder)
                    , cities[i], citiesDescriptions[i]));
        }
        imagesResources.recycle();
        cityRViewAdapter = new CityRViewAdapter(cityRViewItems, this);
    }

    @Override
    public void onItemClick(int pos) {
        String cityNameSelected = "";
        if (cityRViewAdapter != null && cityRViewAdapter.getItem(pos) != null) {
            cityNameSelected = cityRViewAdapter.getItem(pos).getCity();
        }

        //-- save Name City
        presenter.saveSelectCity(cityNameSelected);

        // Track "select city"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CITY_SELECTION.getValue(),
                AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                CityData.cityName());

        onPreviousScreen();
    }

    private void onPreviousScreen() {
        Intent intent = getIntent();
        if (mBackAskScreen) {
            intent.putExtra(IntentConstant.ASK_SCREEN_TO_SELECT_CITY, true);
        }
        setResult(ResultCode.RESULT_OK, intent);
        super.onBackPressed();
    }


    @Override
    public void saveCitySuccessful() {

    }


}
