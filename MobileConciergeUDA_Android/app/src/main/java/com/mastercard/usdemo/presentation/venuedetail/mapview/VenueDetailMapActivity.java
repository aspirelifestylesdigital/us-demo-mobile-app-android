package com.mastercard.usdemo.presentation.venuedetail.mapview;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mastercard.usdemo.R;
import com.mastercard.usdemo.common.logic.HtmlUtils;
import com.mastercard.usdemo.domain.model.explore.CityGuideDetailItem;
import com.mastercard.usdemo.domain.model.explore.DiningDetailItem;
import com.mastercard.usdemo.presentation.base.CommonActivity;
import com.mastercard.usdemo.presentation.widget.DialogHelper;
import com.mastercard.usdemo.presentation.widget.ViewUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VenueDetailMapActivity extends CommonActivity {
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wvMap)
    WebView wvMap;
    private String centerURL;
    private DialogHelper dialogHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_detail_map);
        ButterKnife.bind(this);
        setToolbarColor(R.color.colorPrimaryDark);
        dialogHelper = new DialogHelper(this);
        wvMap.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view,
                                      String url,
                                      Bitmap favicon) {
                dialogHelper.showProgress();
                super.onPageStarted(view,
                                    url,
                                    favicon);
            }

            @Override
            public void onPageFinished(WebView view,
                                       String url) {
                dialogHelper.dismissProgress();
                super.onPageFinished(view,
                                     url);
            }
        });
        wvMap.getSettings().setJavaScriptEnabled(true);
        String query = "";

        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().getParcelable("cityGuide") != null) {
                CityGuideDetailItem cityGuideDetailItem = getIntent().getExtras().getParcelable("cityGuide");
                setTitle(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(cityGuideDetailItem.getTitle())));
//                setupWebView(cityGuideDetailItem.getTitle());
//                if (cityGuideDetailItem.city.contains(Beijing)||cityGuideDetailItem.city.contains(beijing)){
//                    try {
//                        query = URLEncoder.encode(cityGuideDetailItem.getQueryBeiJingSearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
//                    Log.v("VenueDetailMapActivity:", "Beijing:"+ query);
//                }else {
                    try {
                        query = URLEncoder.encode(cityGuideDetailItem.getQuerySearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
                    Log.v("VenueDetailMapActivity:", query);
//                }
            }

            if (getIntent().getExtras().getParcelable("dining") != null) {
                DiningDetailItem cityGuideDetailItem = getIntent().getExtras().getParcelable("dining");
                setTitle(Html.fromHtml(cityGuideDetailItem.getTitle()));
//                setupWebView(cityGuideDetailItem.getTitle());
//                if (cityGuideDetailItem.city.contains(Beijing)||cityGuideDetailItem.city.contains(beijing)){
//                    try {
//                        query = URLEncoder.encode(cityGuideDetailItem.getQuerySearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
//                    Log.v("VenueDetailMapActivity:", "Beijing:"+ query);
//                }else {
                    try {
                        query = URLEncoder.encode(cityGuideDetailItem.getQuerySearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
                    Log.v("VenueDetailMapActivity:", "http://maps.google.com/maps?" + "q="+query);

//                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        Drawable icConcierge = menu.findItem(R.id.action_ask_concierge).getIcon();
        ViewUtils.menuTintColors(this, icConcierge);
        return true;
    }

}
