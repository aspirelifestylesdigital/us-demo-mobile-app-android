package com.support.mylibrary.common;

import android.content.Context;

/**
 * Created by tung.phan on 5/26/2017.
 */

public class Utils {

    public static int dip2px(Context context, float dpValue) {
        if (context != null) {
            final float scale = context.getResources().getDisplayMetrics().density;
            return (int) (dpValue * scale + 0.5f);
        } else {
            return 0;
        }
    }
}
