package com.api.aspire.data.repository;

import com.api.aspire.data.datasource.RemoteForgotPasswordDataStore;
import com.api.aspire.data.entity.forgotpassword.ForgotPasswordRequest;
import com.api.aspire.domain.repository.ForgotPasswordRepository;

import io.reactivex.Completable;

public class ForgotPasswordDataRepository implements ForgotPasswordRepository {
    @Override
    public Completable forgotPassword(String clientIdEmail, String password, String answer) {
        RemoteForgotPasswordDataStore remote = new RemoteForgotPasswordDataStore();
        return remote.forgotPassword(clientIdEmail, buildForgotPasswordRequest(password, answer));
    }

    private ForgotPasswordRequest buildForgotPasswordRequest(String password, String answer) {
        ForgotPasswordRequest request = new ForgotPasswordRequest();

        request.setPassword(new ForgotPasswordRequest.Password(password));
        request.setRecoveryQuestion(new ForgotPasswordRequest.RecoveryQuestion(answer));

        return request;
    }
}
