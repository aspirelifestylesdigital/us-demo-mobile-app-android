package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.repository.ForgotPasswordDataRepository;
import com.api.aspire.domain.repository.ForgotPasswordRepository;
import com.api.aspire.domain.repository.UserProfileOKTARepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class RetrievePassword extends UseCase<Void, RetrievePassword.Param> {

    private MapProfileBase mapLogic;
    private ForgotPasswordRepository forgotPasswordRepository;
    private UserProfileOKTARepository profileOKTARepository;

    public RetrievePassword(MapProfileBase mapLogic,
                            ForgotPasswordDataRepository forgotPasswordRepository,
                            UserProfileOKTARepository userProfileOKTADataRepository) {
        this.mapLogic = mapLogic;
        this.forgotPasswordRepository = forgotPasswordRepository;
        profileOKTARepository = userProfileOKTADataRepository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Param s) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Param s) {
        return null;
    }

    Completable buildUseCaseCompletable(Param param) {
        String clientIdEmail = mapLogic.convertClientIdEmail(param.email);
        return profileOKTARepository.getUserProfileOKTA(clientIdEmail)
                .flatMapCompletable(userProfileOKTA -> {

                    //-- check status lock
                    String status = userProfileOKTA.getStatus();
                    if (profileOKTARepository.isStatusLockOut(status)) {
                        return Completable.error(new Exception(ErrCode.PROFILE_OKTA_LOCKED_OUT.name()));
                    }

                    if (userProfileOKTA.getCredentials() != null
                            && userProfileOKTA.getCredentials().getRecoveryQuestion() != null) {

                        String question = userProfileOKTA
                                .getCredentials()
                                .getRecoveryQuestion()
                                .getQuestion();

                        if (!TextUtils.isEmpty(question) && question.equals(param.recoveryQuestion)) {
                            return forgotPasswordRepository.forgotPassword(clientIdEmail, param.password, param.answer);
                        }
                    }
                    return Completable.error(new Exception(ErrCode.FORGOT_PASSWORD_WRONG_QUESTION.name()));
                });
    }

    public static class Param {
        public String email;
        public String password;
        public String answer;
        public String recoveryQuestion;

        public Param(String email, String password, String recoveryQuestion, String answer) {
            this.email = email;
            this.password = password;
            this.recoveryQuestion = recoveryQuestion;
            this.answer = answer;
        }
    }
}