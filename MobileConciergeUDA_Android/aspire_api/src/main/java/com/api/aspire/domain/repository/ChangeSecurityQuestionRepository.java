package com.api.aspire.domain.repository;

import io.reactivex.Completable;

public interface ChangeSecurityQuestionRepository {

    Completable changeSecurityQuestion(String email,
                                       String question,
                                       String answer,
                                       String password);
}
