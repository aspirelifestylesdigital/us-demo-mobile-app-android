package com.api.aspire.domain.mapper.profile;

import android.text.TextUtils;

import com.api.aspire.data.entity.profile.ProfileAspireResponse;

import java.util.ArrayList;
import java.util.List;

public class ProfileLogicCore {
    //<editor-fold desc="UserAppPreference">
    /**
     * @param key: ConstantAspireApi.APP_USER_PREFERENCES.
     * */
    public static void setAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences,
                                            String key, String value) {
        if (TextUtils.isEmpty(key) || appUserPreferences == null) {
            return;
        }

        boolean isExistPreference = false;

        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                preference.setPreferenceValue(value);
                isExistPreference = true;
                break;
            }
        }

        if (!isExistPreference) {
            ProfileAspireResponse.AppUserPreferences preference = new ProfileAspireResponse.AppUserPreferences(key, value);
            appUserPreferences.add(preference);
        }
    }

    /**
     * func update new value, don't init new userAppPreference
     * @param key: ConstantAspireApi.APP_USER_PREFERENCES.
     * */
    public static void updateAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences,
                                            String key, String value) {
        if (TextUtils.isEmpty(key) || appUserPreferences == null
                || appUserPreferences.isEmpty()) {
            return;
        }

        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                preference.setPreferenceValue(value);
                break;
            }
        }
    }

    /**
     * @param key: ConstantAspireApi.APP_USER_PREFERENCES.
     * */
    public static String getAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences, String key) {
        if (TextUtils.isEmpty(key)) {
            return "";
        }
        if (appUserPreferences == null || appUserPreferences.isEmpty()) return "";


        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                return preference.getPreferenceValue();
            }
        }
        return "";
    }

    public static List<ProfileAspireResponse.AppUserPreferences> isAppUserPrEmpty(List<ProfileAspireResponse.AppUserPreferences> appUserPrValue){
        return appUserPrValue == null ? new ArrayList<>() : appUserPrValue;
    }

    /**
     * default location status is: OFF
     */
    public static String isUserLocation(boolean locationOn) {
        return locationOn ? "ON" : "OFF";
    }

    public static boolean statusUserLocation(String locationValueOn) {
        return "ON".equalsIgnoreCase(locationValueOn);
    }

    //</editor-fold>
}
