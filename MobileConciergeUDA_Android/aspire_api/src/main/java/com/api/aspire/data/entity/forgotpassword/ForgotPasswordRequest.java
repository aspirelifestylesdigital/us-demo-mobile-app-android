package com.api.aspire.data.entity.forgotpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotPasswordRequest {

    @Expose
    @SerializedName("recovery_question")
    private RecoveryQuestion recoveryQuestion;
    @Expose
    @SerializedName("password")
    private Password password;

    public RecoveryQuestion getRecoveryQuestion() {
        return recoveryQuestion;
    }

    public void setRecoveryQuestion(RecoveryQuestion recoveryQuestion) {
        this.recoveryQuestion = recoveryQuestion;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public static class RecoveryQuestion {
        @Expose
        @SerializedName("answer")
        private String answer;

        public RecoveryQuestion(String answer) {
            this.answer = answer;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }
    }

    public static class Password {
        @Expose
        @SerializedName("value")
        private String value;

        public Password(String password) {
            this.value = password;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
