package com.api.aspire.data.entity.recoveryquestion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecoveryQuestionResponse {

    @Expose
    @SerializedName("provider")
    private Provider provider;
    @Expose
    @SerializedName("recovery_question")
    private RecoveryQuestion recoveryQuestion;
    @Expose
    @SerializedName("password")
    private Password password;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public RecoveryQuestion getRecoveryQuestion() {
        return recoveryQuestion;
    }

    public void setRecoveryQuestion(RecoveryQuestion recoveryQuestion) {
        this.recoveryQuestion = recoveryQuestion;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public static class Provider {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("type")
        private String type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class RecoveryQuestion {
        @Expose
        @SerializedName("question")
        private String question;

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }
    }

    public static class Password {
    }
}
