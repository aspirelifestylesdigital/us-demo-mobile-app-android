package com.api.aspire.common.utils;

import android.text.TextUtils;
import android.util.Log;

public class CommonUtils {

    /** return -1 can not parse String to int */
    public static int convertStringToInt(String str) {
        if(TextUtils.isEmpty(str)){
            return -1;
        }

        int number = -1;
        try {
            number = Integer.valueOf(str);
        } catch (Exception ignored) {
            Log.d("TAG", "convertStringToInt: "+ str);
        }

        return number;
    }

}
