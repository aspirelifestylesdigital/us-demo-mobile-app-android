package com.api.aspire.data.entity.askconcierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class ConciergeCaseAspireResponse {

    @SerializedName("errorCode")
    private String errorCode;

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void empty() {
        message = "";
        errorCode = "";
    }

    public boolean isSuccess() {
        return "Case request sent".equalsIgnoreCase(message);
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(message) && TextUtils.isEmpty(errorCode);
    }
}