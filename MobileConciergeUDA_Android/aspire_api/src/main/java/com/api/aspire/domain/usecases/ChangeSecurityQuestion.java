package com.api.aspire.domain.usecases;

import com.api.aspire.domain.model.UserProfileOKTA;
import com.api.aspire.domain.repository.ChangeSecurityQuestionRepository;
import com.api.aspire.domain.repository.UserProfileOKTARepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class ChangeSecurityQuestion extends UseCase<Completable, ChangeSecurityQuestion.Param> {

    private UserProfileOKTARepository userProfileOKTARepository;
    private ChangeSecurityQuestionRepository changeSecurityQuestionRepository;
    private MapProfileBase mapLogic;

    public ChangeSecurityQuestion(UserProfileOKTARepository userProfileOKTARepository,
                                  ChangeSecurityQuestionRepository changeSecurityQuestionRepository,
                                  MapProfileBase mapLogic) {
        this.userProfileOKTARepository = userProfileOKTARepository;
        this.changeSecurityQuestionRepository = changeSecurityQuestionRepository;
        this.mapLogic = mapLogic;
    }

    @Override
    Observable<Completable> buildUseCaseObservable(Param param) {
        return null;
    }

    @Override
    Single<Completable> buildUseCaseSingle(Param param) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Param param) {
        return buildCaseChangeSecurityQuestion(param);
    }

    private Completable buildCaseChangeSecurityQuestion(Param param) {
        return getUserProfileOKTA(param.email)
                .flatMapCompletable(userProfileOKTA ->
                        this.updateChangeQuestion(userProfileOKTA,
                                param.question,
                                param.answer,
                                param.secret)
                );
    }

    public Single<UserProfileOKTA> getUserProfileOKTA(String email){
        String clientIdEmail = mapLogic.convertClientIdEmail(email);
        return userProfileOKTARepository.getUserProfileOKTA(clientIdEmail);
    }

    public Completable updateChangeQuestion(UserProfileOKTA userProfileOKTA,
                                            String question,
                                            String answer,
                                            String secret) {

        String userId = userProfileOKTA == null ? "" : userProfileOKTA.getId();
        return changeSecurityQuestionRepository.changeSecurityQuestion(userId,
                question,
                answer,
                secret
        );
    }

    public static class Param {
        public String email;
        public String secret;
        public String question;
        public String answer;

        public Param(String email, String secret, String question, String answer) {
            this.email = email;
            this.secret = secret;
            this.question = question;
            this.answer = answer;
        }
    }
}
