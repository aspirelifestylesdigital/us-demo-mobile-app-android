package com.api.aspire.data.entity.passcode;

import com.google.gson.annotations.SerializedName;

public class PassCodeResponse {
    @SerializedName("result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
